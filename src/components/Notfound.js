import React from 'react';
import { Status } from '../routes';

const NotFound = () => (
  <Status code={404}>
    Error
  </Status>
);

export default NotFound;
