import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';


const YearContainer = styled.div`
   overflow-x:auto;
   position: relative;
   margin-left:10px;
   margin-right:10px;
   padding-bottom:2px;
     white-space: nowrap;
     ::-webkit-scrollbar {
           height: 5px;
           background-color:#3F51B5;
     };
     ::-webkit-scrollbar-track {
       -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
border-radius: 10px;
background-color: #9FA8DA;
     };
     ::-webkit-scrollbar-thumb {
       border-radius: 10px;
-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
background-color: #336188;
     };
`;
const YearItem = styled.div`
   display:inline-block;
   padding:10px;
   position:relative;
   cursor:pointer;
   ${props => (props.selected ? 'border-bottom: 2px solid rgb(72, 72, 72)' : '')}
`;

const FilterPerYear = ({
  filterClick, genero, platform, year, showType,
}) => {
  const getYears = () => {
    const endYear = 1950;
    let startYear = new Date().getFullYear();
    const yearArray = [];

    yearArray.push('all');
    while (startYear >= endYear) {
      yearArray.push(startYear);
      startYear--;
    }
    return yearArray.map(y => (
      <YearItem selected={year[y] === true} key={y} onClick={() => filterClick('year', y)}>
        {(y === 'all') ? 'Todas' : y}
      </YearItem>
    ));
  };
  return (
    <YearContainer>
      {getYears()}
    </YearContainer>
  );
};

export default FilterPerYear;
