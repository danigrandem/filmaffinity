import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InitialLoading extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    setTimeout(() => {
      dispatch({ type: 'HIDE_LOADING', isSsr: false });
    }, 1000);
  }
  render() {
    return (
      <div id="loader-wrapper">
        <div id="loader" />
        <div className="loader-section section-left" />
        <div className="loader-section section-right" />
      </div>
    );
  }
}
InitialLoading.propTypes = {
  dispatch: PropTypes.func.isRequired,
};
export default InitialLoading;
