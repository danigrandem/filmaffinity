import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import Film from "./Film";

const H3 = styled.h3`
display:block;
text-transform: uppercase;
font-size: 18px;
border-bottom: 1px solid black;
padding: 5px 15px;
margin-bottom:15px;
font-weight:bold;
margin-top:-5px
`;
const FilContainer = styled.div`
margin-right:10px;
vertical-align:top;
width:100%;
display:inline-block;


@media (min-width: 500px) {
  width:48%
};
@media (min-width: 900px) {
  width:24%
};
}
`;
const FilmList = ({
  films,
  onFilmClick,
  idFilm,
  onShowMoreInfo,
  idFilmMore,
  isWebView,
  filters,
  showList
}) => {
  const dates = [];
  const getFilms = () =>
    films.map((film, i) => {
      const d = new Date(film.get("inserted_at")).toLocaleDateString("en-GB");
      const date = !showList && filters.get('order') === 'new_releases' && !dates.includes(d)?d:'';
      date !== '' && dates.push(d);
      const retorn = [];
      date !== '' && retorn.push(<H3>{d}</H3>);
      retorn.push(<FilContainer key={`${film.get("id")}`} className="item">
          <Link
            to={`/ficha/${film.get("id")}`}
            title={`Ver ${film.get("name")} online`}
          >
            <Film
              isWebView={isWebView}
              film={film}
              onFilmClick={onFilmClick}
              idFilm={idFilm}
              onShowMoreInfo={onShowMoreInfo}
              idFilmMore={idFilmMore}
            />
          </Link>
      </FilContainer>);
      return retorn;
    });
  return getFilms();
};

FilmList.propTypes = {
  films: PropTypes.object.isRequired,
  onFilmClick: PropTypes.func.isRequired,
  idFilm: PropTypes.string.isRequired,
  onShowMoreInfo: PropTypes.func.isRequired,
  idFilmMore: PropTypes.string.isRequired
};
export default FilmList;
