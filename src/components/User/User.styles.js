import styled from 'styled-components';

export const UserContainer = styled.div`
position:fixed;
margin-left:-10px;
background:white;
top:55px;
width:100%;
height:100%;
`;
export const UserInfo = styled.div`
text-align:center
`;

export const TabList = styled.div`
width:100%;
margin-top:10px
`;
export const Avatar = styled.img`
border-radius:30px;
margin-top:10px;
width:70px
`;

export const UserName = styled.span`

`;
export const TabTitle = styled.span`
width:50%;
text-align:center;
display:inline-block
`;

export const TabContent = styled.div`

`;
