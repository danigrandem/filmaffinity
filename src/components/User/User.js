import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { UserInfo, Avatar, UserName, TabList, TabTitle, TabContent, UserContainer } from './User.styles';


class User extends Component {
  render() {
    const { email, fullName, avatar } = this.props;
    return (
      <UserContainer>
        <UserInfo>
          <Avatar src={avatar} /><br/>
          <UserName>{fullName}</UserName>
        </UserInfo>
        <TabList>
          <TabTitle>Quiero ver</TabTitle>
          <TabTitle>He visto</TabTitle>
          <TabContent />
          <TabContent />
        </TabList>
      </UserContainer>
    );
  }
}

export default User;
