import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Autosuggest from 'react-autosuggest';
import './Header.css';

class Search extends Component {
  constructor() {
    super();

    // Autosuggest is a controlled component.
    // This means that you need to provide an input value
    // and an onChange handler that updates this value (see below).
    // Suggestions also need to be provided to the Autosuggest,
    // and they are initially empty because the Autosuggest is closed.
    this.state = {
      value: '',
      suggestions: [],
    };
  }
  componentWillReceiveProps(nextProps) {
    const { value, suggestions } = this.state;
    if (nextProps.search && nextProps.search.get(value) && suggestions.length === 0) {
      this.setState({
        suggestions: this.getArray(nextProps.search.get(value)).toArray(),
      });
    }
  }
  getArray = data =>
    data.map(d => ( d.toJS() ));

  getSuggestions = (value) => {
    const inputValue = value.toLowerCase();
    const inputLength = inputValue.length;
    const search = this.props.search.get(inputValue);

    return !search || inputLength === 0 ? [] : this.getArray(search).toArray();
  };
  getSuggestionValue = suggestion => suggestion.name;
  onChange = (event, { newValue }) => {
    newValue.length > 0 && this.props.dispatch({ type: 'ITEM_SEARCH', value: newValue.toLowerCase() });
    this.setState({
      value: newValue.toLowerCase(),
    });
  };

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value),
    });
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
      value: ''
    });
  };
  onSelect = () => {
    this.props.suggestionSelected();
    this.onSuggestionsClearRequested();
  }
  renderSuggestion = suggestion => (
    <div>
      <img src={suggestion.image} /><div className="search-info"><span>{suggestion.name}</span><br/><em>{suggestion.year}</em></div>
    </div>
  );

  render() {
    const { value, suggestions } = this.state;
    // Autosuggest will pass through all these props to the input.
    const inputProps = {
      placeholder: 'Buscar por nombre, actores, director...',
      value,
      onChange: this.onChange,
    };
    // Finally, render it!
    return (
      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        getSuggestionValue={this.getSuggestionValue}
        renderSuggestion={this.renderSuggestion}
        inputProps={inputProps}
        onSuggestionSelected={this.props.suggestionSelected}
      />
    );
  }
}

Search.propTypes = {
};
export default Search;
