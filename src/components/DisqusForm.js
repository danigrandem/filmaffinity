import React from 'react';
import Disqus from 'disqus-react';

const DisqusForm = (film) => {
  const disqusShortname = 'filmchart-es';
  const disqusConfig = {
    url: `https://filmchart.es/ficha/${film.id}`,
    identifier: film.id,
    title: film.name,
  };
  return (
    <div className="article">
      <h1>{film.name}</h1>
      <Disqus.CommentCount shortname={disqusShortname} config={disqusConfig} />
      <Disqus.DiscussionEmbed shortname={disqusShortname} config={disqusConfig} />
    </div>
  );
};

export default DisqusForm;
