import React, { Component } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { slide as Menu } from "react-burger-menu";
import { Link } from "react-router-dom";
import scrollTo from "../scrollTo";
import "./Header.css";
import Search from "./Search";

const DivHeaderWebView = styled.div`
  position: fixed;
  width: 100%;
  z-index: 999;
  color: white;
  box-shadow: rgb(204, 204, 204) 2px 2px 4px 0px;
  > div:first-child {
    background-color: #3f51b5;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 10px;
    padding-bottom: 10px;
  }
`;
const WebViewHeaderTitle = styled.div`
  display: inline-block;
  font-size: 30px;
  font-weight: bold;
  cursor: pointer;
  ${props =>
    props.isLogged ? "padding-left: 75px;" : ""} @media (max-width: 350px) {
    font-size: 20px;
  }
`;
const OrderContainer = styled.div`
  display: inline-block;
  left: 0px;
  height: 35px;
  width: 100%;
  background-color: white;
  text-align: center;
  box-shadow: rgb(204, 204, 204) 2px 2px 4px 0px;
  line-height: 30px;

  position: absolute;
`;
const HeaderPadding = styled.div`
  ${props =>
    !props.filmView ? "padding-bottom:100px" : "padding-bottom:50px"};
`;
const ActionContainer = styled.div`
  display: inline-block;
  line-height: 35px;
  margin-left: 10px;
`;
const HeaderContainer = styled.div`
  display: flex;
`;
const ButtonOrderNewReleases = styled.button`
  color: black;
  font-weight: bold;
  display: inline;
  cursor: pointer;
  background: none;
  border: none;
  font-size: 14px;
  ${props =>
    props.isOrderNewReleases
      ? "border-bottom:1px solid black;"
      : ";"} &:active {
    outline: 0;
    background: #7c8adb;
  }
`;
const ButtonOrderIMDB = styled.button`
  color: black;
  font-weight: bold;
  display: inline;
  cursor: pointer;
  background: none;
  border: none;
  font-size: 14px;
  ${props =>
    props.isOrderIMDB ? "border-bottom:1px solid black;" : ";"} &:active {
    outline: 0;
    background: #7c8adb;
  }
`;
const ButtonOrderFA = styled.button`
  color: black;
  font-weight: bold;
  display: inline;
  cursor: pointer;
  background: none;
  border: none;
  font-size: 14px;
  ${props =>
    props.isOrderFA ? "border-bottom:1px solid black;" : ";"} &:active {
    outline: 0;
    background: #7c8adb;
  }
`;
const ImgBack = styled.img`
  width: 30px;
  height: 30px;
  cursor: pointer;
  color: white;
`;
const Avatar = styled.img`
  width: 72px;
  height: 72px;
  display: block;
  border-radius: 50px;
`;

const SearchButton = styled.button`
  width:35px;
  height:35px;
  cursor:pointer;
  color:white;
background-image: url(data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMS4xLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDI1MC4zMTMgMjUwLjMxMyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMjUwLjMxMyAyNTAuMzEzOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjMycHgiIGhlaWdodD0iMzJweCI+CjxnIGlkPSJTZWFyY2giPgoJPHBhdGggc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkOyIgZD0iTTI0NC4xODYsMjE0LjYwNGwtNTQuMzc5LTU0LjM3OGMtMC4yODktMC4yODktMC42MjgtMC40OTEtMC45My0wLjc2ICAgYzEwLjctMTYuMjMxLDE2Ljk0NS0zNS42NiwxNi45NDUtNTYuNTU0QzIwNS44MjIsNDYuMDc1LDE1OS43NDcsMCwxMDIuOTExLDBTMCw0Ni4wNzUsMCwxMDIuOTExICAgYzAsNTYuODM1LDQ2LjA3NCwxMDIuOTExLDEwMi45MSwxMDIuOTExYzIwLjg5NSwwLDQwLjMyMy02LjI0NSw1Ni41NTQtMTYuOTQ1YzAuMjY5LDAuMzAxLDAuNDcsMC42NCwwLjc1OSwwLjkyOWw1NC4zOCw1NC4zOCAgIGM4LjE2OSw4LjE2OCwyMS40MTMsOC4xNjgsMjkuNTgzLDBDMjUyLjM1NCwyMzYuMDE3LDI1Mi4zNTQsMjIyLjc3MywyNDQuMTg2LDIxNC42MDR6IE0xMDIuOTExLDE3MC4xNDYgICBjLTM3LjEzNCwwLTY3LjIzNi0zMC4xMDItNjcuMjM2LTY3LjIzNWMwLTM3LjEzNCwzMC4xMDMtNjcuMjM2LDY3LjIzNi02Ny4yMzZjMzcuMTMyLDAsNjcuMjM1LDMwLjEwMyw2Ny4yMzUsNjcuMjM2ICAgQzE3MC4xNDYsMTQwLjA0NCwxNDAuMDQzLDE3MC4xNDYsMTAyLjkxMSwxNzAuMTQ2eiIgZmlsbD0iI0ZGRkZGRiIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=)};
background-size:28px;
background-repeat:no-repeat;
background-color:rgba(0,0,0,0);
transition: transform .2s;
${props => (props.showSearch ? "transform: scale(1.2);" : ";")}
border:0px

`;
const OrderButton = styled.button`
  width:35px;
  height:35px;
  cursor:pointer;
  color:white;
background-image: url("/media/filter.svg")};
transition: transform .2s;
${props => (props.showOrder ? "transform: scale(1.2);" : ";")}
background-size:28px;
background-repeat:no-repeat;
background-color:rgba(0,0,0,0);
border:0px

`;
class Header extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showSearch: false,
      showOrders: true,
      showMenu: false
    };
  }
  componentDidMount() {
    window.addEventListener("mousedown", this.handleClickHeader, false);
  }
  componentDidUnMount() {
    window.removeEventListener("mousedown", this.handleClickHeader, false);
  }
  componentWillReceiveProps(nextProps) {
    if (this.state.showOrder === true) {
      this.setState({
        showOrder: false
      });
    }
  }
  handleClickHeader = e => {
    if (this.node !== null && this.node.contains(e.target)) {
      return;
    }
    this.setState({
      showSearch: false
    });
  };

  suggestionSelected = (
    event,
    { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }
  ) => {
    this.setState({
      showSearch: false
    });
    this.props.onFilmSearch(suggestion);
  };
  onSearchClick = clicked => {
    this.setState({
      showSearch: clicked
    });
  };
  onOrderClick = () => {
    this.setState({
      showOrder: !this.state.showOrder
    });
  };
  showOrderContainer = (filters, onOrderChange) => (
    <OrderContainer showOrder={this.state.showOrder}>
      <ButtonOrderNewReleases
        isOrderNewReleases={filters.get("order") === "new_releases"}
        onClick={() => onOrderChange("new_releases")}
      >
        Novedades
      </ButtonOrderNewReleases>
      <ButtonOrderFA
        isOrderFA={filters.get("order") === "note_filmaffinity"}
        onClick={() => onOrderChange("note_filmaffinity")}
      >
        Filmaffinity
      </ButtonOrderFA>
      <ButtonOrderIMDB
        isOrderIMDB={filters.get("order") === "note_imdb"}
        onClick={() => onOrderChange("note_imdb")}
      >
        IMDB
      </ButtonOrderIMDB>
    </OrderContainer>
  );
  showHeader = (
    filters,
    onOrderChange,
    onFilmClick,
    filmView,
    onFilmSearch,
    onFilterClick
  ) => {
    const { user } = this.props;
    let header = "";
    if (!filmView) {
      header = (
        <HeaderContainer
          ref={node => {
            this.node = node;
            return this.node;
          }}
        >
          {user.get("email") &&
            !filmView &&
            !this.state.showSearch && (
              <Menu
                onStateChange={state => this.handleStateChange(state)}
                isOpen={this.state.showMenu}
                customBurgerIcon={
                  <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDUzIDUzIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MyA1MzsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI1MTJweCIgaGVpZ2h0PSI1MTJweCI+CjxnPgoJPGc+CgkJPHBhdGggZD0iTTIsMTMuNWg0OWMxLjEwNCwwLDItMC44OTYsMi0ycy0wLjg5Ni0yLTItMkgyYy0xLjEwNCwwLTIsMC44OTYtMiwyUzAuODk2LDEzLjUsMiwxMy41eiIgZmlsbD0iI0ZGRkZGRiIvPgoJCTxwYXRoIGQ9Ik0yLDI4LjVoNDljMS4xMDQsMCwyLTAuODk2LDItMnMtMC44OTYtMi0yLTJIMmMtMS4xMDQsMC0yLDAuODk2LTIsMlMwLjg5NiwyOC41LDIsMjguNXoiIGZpbGw9IiNGRkZGRkYiLz4KCQk8cGF0aCBkPSJNMiw0My41aDQ5YzEuMTA0LDAsMi0wLjg5NiwyLTJzLTAuODk2LTItMi0ySDJjLTEuMTA0LDAtMiwwLjg5Ni0yLDJTMC44OTYsNDMuNSwyLDQzLjV6IiBmaWxsPSIjRkZGRkZGIi8+Cgk8L2c+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==" />
                }
              >
                <div className="user-info">
                  <Avatar src={user.get("avatar")} />
                  <p className="user-name">{user.get("name")}</p>
                </div>
                <ul className="menu-links">
                  <li>
                    <Link
                      id="home"
                      className="menu-item"
                      to="/"
                      onClick={() => {
                        onFilterClick(
                          "all",
                          "all",
                          "all",
                          "all",
                          "all",
                          "new_releases"
                        );
                        this.setState({
                          showMenu: false
                        });
                      }}
                    >
                      <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjMycHgiIGhlaWdodD0iMzJweCIgdmlld0JveD0iMCAwIDQ5NS4zOTggNDk1LjM5OCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDk1LjM5OCA0OTUuMzk4OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPgoJPGc+CgkJPGc+CgkJCTxwYXRoIGQ9Ik00ODcuMDgzLDIyNS41MTRsLTc1LjA4LTc1LjA4VjYzLjcwNGMwLTE1LjY4Mi0xMi43MDgtMjguMzkxLTI4LjQxMy0yOC4zOTFjLTE1LjY2OSwwLTI4LjM3NywxMi43MDktMjguMzc3LDI4LjM5MSAgICAgdjI5Ljk0MUwyOTkuMzEsMzcuNzRjLTI3LjYzOS0yNy42MjQtNzUuNjk0LTI3LjU3NS0xMDMuMjcsMC4wNUw4LjMxMiwyMjUuNTE0Yy0xMS4wODIsMTEuMTA0LTExLjA4MiwyOS4wNzEsMCw0MC4xNTggICAgIGMxMS4wODcsMTEuMTAxLDI5LjA4OSwxMS4xMDEsNDAuMTcyLDBsMTg3LjcxLTE4Ny43MjljNi4xMTUtNi4wODMsMTYuODkzLTYuMDgzLDIyLjk3Ni0wLjAxOGwxODcuNzQyLDE4Ny43NDcgICAgIGM1LjU2Nyw1LjU1MSwxMi44MjUsOC4zMTIsMjAuMDgxLDguMzEyYzcuMjcxLDAsMTQuNTQxLTIuNzY0LDIwLjA5MS04LjMxMkM0OTguMTcsMjU0LjU4Niw0OTguMTcsMjM2LjYxOSw0ODcuMDgzLDIyNS41MTR6IiBmaWxsPSIjMDAwMDAwIi8+CgkJCTxwYXRoIGQ9Ik0yNTcuNTYxLDEzMS44MzZjLTUuNDU0LTUuNDUxLTE0LjI4NS01LjQ1MS0xOS43MjMsMEw3Mi43MTIsMjk2LjkxM2MtMi42MDcsMi42MDYtNC4wODUsNi4xNjQtNC4wODUsOS44Nzd2MTIwLjQwMSAgICAgYzAsMjguMjUzLDIyLjkwOCw1MS4xNiw1MS4xNiw1MS4xNmg4MS43NTR2LTEyNi42MWg5Mi4yOTl2MTI2LjYxaDgxLjc1NWMyOC4yNTEsMCw1MS4xNTktMjIuOTA3LDUxLjE1OS01MS4xNTlWMzA2Ljc5ICAgICBjMC0zLjcxMy0xLjQ2NS03LjI3MS00LjA4NS05Ljg3N0wyNTcuNTYxLDEzMS44MzZ6IiBmaWxsPSIjMDAwMDAwIi8+CgkJPC9nPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" />
                      <span>Inicio</span>
                    </Link>
                  </li>
                  <li>
                    <button
                      className="menu-item"
                      onClick={this.onShowListClick}
                    >
                      <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQ4NSA0ODUiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQ4NSA0ODU7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iMzJweCIgaGVpZ2h0PSIzMnB4Ij4KPGc+Cgk8Zz4KCQk8cGF0aCBkPSJNNDgzLjgsNDYuMjVIMTgxLjZjLTAuNiwwLTEuMiwwLjUtMS4yLDEuMnYyOC4zYzAsMC42LDAuNSwxLjIsMS4yLDEuMmgzMDIuMmMwLjYsMCwxLjItMC41LDEuMi0xLjJ2LTI4LjMgICAgQzQ4NSw0Ni43NSw0ODQuNSw0Ni4yNSw0ODMuOCw0Ni4yNXoiIGZpbGw9IiMwMDAwMDAiLz4KCQk8Y2lyY2xlIGN4PSI3MS41IiBjeT0iMjUzLjM1IiByPSIzNy45IiBmaWxsPSIjMDAwMDAwIi8+CgkJPGNpcmNsZSBjeD0iNzEuNSIgY3k9IjQxOS41NSIgcj0iMzguMSIgZmlsbD0iIzAwMDAwMCIvPgoJCTxwYXRoIGQ9Ik0xODEuNiwxMjguMTVoMTQyLjhjMC42LDAsMS4yLTAuNSwxLjItMS4ydi0yOC4zYzAtMC42LTAuNS0xLjItMS4yLTEuMkgxODEuNmMtMC42LDAtMS4yLDAuNS0xLjIsMS4ydjI4LjMgICAgQzE4MC41LDEyNy42NSwxODEsMTI4LjE1LDE4MS42LDEyOC4xNXoiIGZpbGw9IiMwMDAwMDAiLz4KCQk8cGF0aCBkPSJNNDgzLjgsMjEyLjQ1SDE4MS42Yy0wLjYsMC0xLjIsMC41LTEuMiwxLjJ2MjguM2MwLDAuNiwwLjUsMS4yLDEuMiwxLjJoMzAyLjJjMC42LDAsMS4yLTAuNSwxLjItMS4ydi0yOC4zICAgIEM0ODUsMjEyLjk1LDQ4NC41LDIxMi40NSw0ODMuOCwyMTIuNDV6IiBmaWxsPSIjMDAwMDAwIi8+CgkJPHBhdGggZD0iTTE4MS42LDI5NC4zNWgxNDIuOGMwLjYsMCwxLjItMC41LDEuMi0xLjJ2LTI4LjNjMC0wLjYtMC41LTEuMi0xLjItMS4ySDE4MS42Yy0wLjYsMC0xLjIsMC41LTEuMiwxLjJ2MjguMyAgICBDMTgwLjUsMjkzLjc1LDE4MSwyOTQuMzUsMTgxLjYsMjk0LjM1eiIgZmlsbD0iIzAwMDAwMCIvPgoJCTxwYXRoIGQ9Ik00ODMuOCwzNzguNTVIMTgxLjZjLTAuNiwwLTEuMiwwLjUtMS4yLDEuMnYyOC4zYzAsMC42LDAuNSwxLjIsMS4yLDEuMmgzMDIuMmMwLjYsMCwxLjItMC41LDEuMi0xLjJ2LTI4LjMgICAgQzQ4NSwzNzkuMDUsNDg0LjUsMzc4LjU1LDQ4My44LDM3OC41NXoiIGZpbGw9IiMwMDAwMDAiLz4KCQk8cGF0aCBkPSJNMzI0LjQsNDI5Ljg1SDE4MS42Yy0wLjYsMC0xLjIsMC41LTEuMiwxLjJ2MjguM2MwLDAuNiwwLjUsMS4yLDEuMiwxLjJoMTQyLjhjMC42LDAsMS4yLTAuNSwxLjItMS4ydi0yOC4zICAgIEMzMjUuNiw0MzAuMzUsMzI1LjEsNDI5Ljg1LDMyNC40LDQyOS44NXoiIGZpbGw9IiMwMDAwMDAiLz4KCQk8cG9seWdvbiBwb2ludHM9IjUzLjQsODYuNTUgMjcuMyw2MC4yNSAwLDg3LjQ1IDI2LjIsMTEzLjY1IDUzLjMsMTQwLjk1IDgwLjUsMTEzLjg1IDE0Myw1MS43NSAxMTUuOSwyNC40NSAgICIgZmlsbD0iIzAwMDAwMCIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" />
                      <span>Mi lista</span>
                    </button>
                  </li>
                </ul>
              </Menu>
            )}
          <Search
            suggestionSelected={this.suggestionSelected}
            dispatch={this.props.dispatch}
            search={this.props.search}
          />
          <ActionContainer>
            <OrderButton
              showOrder={this.state.showOrder}
              title="Ordenar por"
              onClick={() => {
                this.props.onFilterModalClick(true);
                this.props.history.push("#menu");
              }}
            >
              &nbsp;
            </OrderButton>
          </ActionContainer>
        </HeaderContainer>
      );
    } else {
      header = (
        <HeaderContainer
          ref={node => {
            this.node = node;
            return this.node;
          }}
        >
          <ImgBack
            isLogged={this.props.user.get("email") !== ""}
            onClick={() =>
              this.props.goBack(
                filters.get("genero"),
                filters.get("platform"),
                filters.get("note"),
                filters.get("year"),
                filters.get("showType"),
                filters.get("order")
              )
            }
            src="/media/images/back.png"
          />
          <Search
            suggestionSelected={this.suggestionSelected}
            dispatch={this.props.dispatch}
            search={this.props.search}
          />
        </HeaderContainer>
      );
    }
    return header;
  };
  onShowListClick = () => {
    document.getElementById("body").className = "";
    this.setState({
      showMenu: false
    });
    scrollTo(0, 0);
    this.props.dispatch({ type: "SET_SHOW_LIST", showList: true });
  };
  handleStateChange(state) {
    this.setState({ showMenu: state.isOpen });
  }
  render() {
    const {
      user,
      filters,
      onOrderChange,
      onFilmClick,
      filmView,
      onFilmSearch,
      onFilterClick,
      search
    } = this.props;
    return (
      <HeaderPadding filmView={filmView}>
        <DivHeaderWebView>
          <div>
            {this.showHeader(
              filters,
              onOrderChange,
              onFilmClick,
              filmView,
              onFilmSearch,
              onFilterClick
            )}
          </div>
          {!filmView && this.showOrderContainer(filters, onOrderChange)}
        </DivHeaderWebView>
      </HeaderPadding>
    );
  }
}

export default Header;
