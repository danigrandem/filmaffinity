import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';


const Range = styled.input`
  -webkit-appearance: none;
  background:#DDDDDD;
  height:3px;
  width:100%;
  &::-webkit-slider-thumb{
    -webkit-appearance: none !important;
    background-color: #E9E9E9;
    border: 1px solid #CECECE;
    height: 15px;
    width: 15px;
    border-radius:10px
  }
`;
const NotesContainer = styled.div`
`;
const RangeContainer = styled.div`
`;
const SpanNumber = styled.span`
display: inline-block;
width: 9%;
${props => ((props.number > 5) ? 'text-align:right' : 'text-align:center')};
${props => ((props.number === 6) ? 'margin-left:-3px' : '')};
${props => ((props.number === 9) ? 'margin-left:3px' : '')}
`;
const Span = styled.span`

`;
const RangeSelector = ({ handleNoteChange, note }) => {
  const notes = () => {
    const notesArray = ['Todas', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    return notesArray.map((n, i) => (
      <SpanNumber key={`${n}`} number={i}>{n}</SpanNumber>
    ));
  };
  return (
    <div>
      <RangeContainer>
        <Range type="range" value={(note === 'all') ? '0' : note} min="0" max="10" onChange={handleNoteChange} />
      </RangeContainer>
      <NotesContainer>
        <Span>
          {notes()}
        </Span>
      </NotesContainer>
    </div>
  );
};
RangeSelector.propTypes = {
  handleNoteChange: PropTypes.func.isRequired,
  note: PropTypes.string.isRequired,
};
export default RangeSelector;
