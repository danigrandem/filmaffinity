import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';


const YearContainer = styled.div`
   overflow-x:auto;
   position: relative;
   margin-left:10px;
   margin-right:10px;
   padding-bottom:2px;
     white-space: nowrap;
     ::-webkit-scrollbar {
           height: 5px;
           background-color:#3F51B5;
     };
     ::-webkit-scrollbar-track {
       -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
border-radius: 10px;
background-color: #9FA8DA;
     };
     ::-webkit-scrollbar-thumb {
       border-radius: 10px;
-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
background-color: #336188;
     };
`;
const YearItem = styled.div`
   display:inline-block;
   padding:10px;
   position:relative;
   cursor:pointer;
   ${props => (props.selected ? 'border-bottom: 2px solid rgb(72, 72, 72)' : '')}
`;

const FilterPerNote = ({
  filterClick, genero, platform, year, showType, note,
}) => {
  const getNotes = () => {
    const endNote = 1;
    let startNote = 10;
    const noteArray = [];

    noteArray.push('all');
    while (startNote >= endNote) {
      noteArray.push(startNote);
      startNote--;
    }
    return noteArray.map(n => (
      <YearItem selected={note[n] === true} key={n} onClick={() => filterClick('note', n)}>
        {(n === 'all') ? 'Todas' : n}
      </YearItem>
    ));
  };
  return (
    <YearContainer>
      {getNotes()}
    </YearContainer>
  );
};

export default FilterPerNote;
