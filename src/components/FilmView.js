import React, { Component } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import FilmGeneros from "./FilmGeneros";
import Header from "./Header";
import DisqusForm from "./DisqusForm";
import "react-accessible-accordion/dist/minimal-example.css";
import "./FilmView.css";
import { getPlatformId } from "../constants";
import {
  Accordion,
  AccordionItem,
  AccordionItemTitle,
  AccordionItemBody
} from "react-accessible-accordion";

const TitleContainer = styled.div`
  background: white;
  box-shadow: 2px 2px 4px 0 #ccc;
  padding: 20px;
  color: #444;
`;
const DirectorContainer = styled.div`
  background: white;
  margin-top: 10px;
  box-shadow: 2px 2px 4px 0 #ccc;
  padding: 20px;
  color: #444;
`;
const MainInfoContainer = styled.div`
  background: white;
  margin-top: 10px;
  box-shadow: 2px 2px 4px 0 #ccc;
  padding: 20px;
  color: #444;
`;
const NoteContainer = styled.div`
  background: white;
  margin-top: 10px;
  box-shadow: 2px 2px 4px 0 #ccc;
  padding: 20px;
  color: #444;
`;
const MoreInfoContainer = styled.a`
  margin-top: 10px;
  box-shadow: 2px 2px 4px 0 #ccc;
  padding: 20px;
  color: #444;
  text-decoration: none;
  display: block;
  background: url(/media/images/next.png) center right no-repeat;
  background-color: white;
`;

const PlatformContainer = styled.div`
  background: white;
  margin-top: 10px;
  box-shadow: 2px 2px 4px 0 #ccc;
  padding: 20px;
  color: #444;
`;

const DivImg = styled.div`
  display: inline-block;
  width: 29%;
  vertical-align: top;
  @media (min-width: 800px) {
    width: 15% !important;
  }
`;
const Img = styled.img`
  width: 100%;
`;
const ActoresContainer = styled.div`
background:white;
margin-top:10px;
box-shadow: 2px 2px 4px 0 #ccc;
padding:20px;
margin-bottom:20px;
color:#444;
overflow-x:auto;
position: relative;
padding-bottom:2px;
  white-space: nowrap;
  ::-webkit-scrollbar {
        height: 5px;
        background-color:#3F51B5;
  };
  ::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
border-radius: 10px;
background-color: #9FA8DA;
}#444;
  ::-webkit-scrollbar-thumb {
    border-radius: 10px;
-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
background-color: #336188;
  };
`;
const ActorItem = styled.div`
  display: inline-block;
  padding: 10px;
  position: relative;
`;
const FilmInfo = styled.div`
  display: inline-block;
  padding: 10px;
  padding-left: 15px;
  width: 60%;
  color: #444;

  @media (min-width: 800px) {
    width: 70% !important;
  }
`;
const ButtonMoreDesc = styled.button`
  background: none;
  border: none;
  cursor: pointer;
`;

const InfoYear = styled.span`
  font-size: 12px;
  color: #aaaaaa;
`;
const H1 = styled.h1`
  margin: 0px;
`;

const LinkPlatform = styled.a`
  display: inline-block;
`;
const ImgPlatforms = styled.img`
  display: block;
  width: 70px;
  height: 70px;
`;
const ViewContainer = styled.div`
  top: 0px;
  width: 100%;
  height: 100%;
  ${props => (!props.playTrailer ? "overflow:auto;" : "")} background: #e9e9e9;
  margin-left: 0px;
  padding-left: 0px;
  left: 0;
  position: fixed;
  ${props =>
    !props.isFilmView
      ? "transform: translateX(-2910px);"
      : "transform: translateX(0px);"} -webkit-transition: -webkit-transform 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
  -moz-transition: -moz-transform 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
  transition: transform 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
`;
const Iframe = styled.iframe`
  width: 90%;
  height: 90%;
`;
const SpanClose = styled.span`
  color: white;
  float: right;
  display: block;
  width: 100%;
  text-align: right;
  margin-right: 10px;
  font-size: 20px;
  font-weight: bold;
  cursor: pointer;
  margin-top: 10px;
`;
const NoteInfo = styled.div`
  line-height: 20px;
  display: inline-block;
  color: #444;
`;
const H3 = styled.h3`
  margin-bottom: 0px;
  padding-left: 20px;
`;
const TrailerContainer = styled.div`
background-position:top;
  background-size: cover;
  height: 120px;
  padding-top: 40px;
  text-align: center;
  background-repeat: no-repeat;
  ${props =>
    props.banner !== undefined
      ? `background-image: url(${props.banner});`
      : ""};
`;
const ButtonList = styled.button`
position:absolute;
background: transparent;
border: none;
outline: 0;
right:20px;
margin-top:-25px;
img{
width:50px;
height:50px;
cursor:pointer;
background: white;
    border-radius: 50px;
}
}
`;
const PlayContainerDiv = styled.div`
  ${props =>
    !props.play
      ? "visibility:hidden;"
      : "visibility:visible;"} transition: visibility 0s, opacity 2s linear;
  width: 100%;
  text-align: center;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 10;
  background-color: rgba(0, 0, 0, 0.8);
`;
const LoadingIframe = styled.div`
  background: url(/media/images/loading.gif) center center no-repeat;
  width: 100%;
  height: 100%;
`;

class FilmView extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showTrailer: false,
      showFullDesc: false
    };
  }
  showMoreInf = () => {
    this.setState({
      showFullDesc: true
    });
  };
  getDescription = desc => {
    let description = "";
    if (!this.state.showFullDesc) {
      description = (
        <FilmInfo onClick={() => this.showMoreInf()}>
          {desc.substring(0, 200)} <ButtonMoreDesc>...</ButtonMoreDesc>
        </FilmInfo>
      );
    } else {
      description = <FilmInfo>{desc}</FilmInfo>;
    }
    return description;
  };

  getPlatforms = platformsSelected =>
    platformsSelected.map(p => (
      <LinkPlatform
        title={`Ver en ${p}`}
        key={`${p}_${this.props.filmView.get("id")}`}
        target="_blank"
        href={this.props.filmView.getIn(["platforms", getPlatformId(p)])}
      >
        <ImgPlatforms alt={`Ver en ${p}`} src={`/media/images/${p}.jpg`} />
      </LinkPlatform>
    ));
  playTrailer = play => {
    this.setState({
      showTrailer: play
    });
  };
  onFilmClickFilmView = idFilm => {
    this.setState({
      showTrailer: false,
      showFullDesc: false
    });
    this.props.onFilmClick(idFilm);
  };
  renderPlatforms = filmView => (
    <div>
      <H3>Reproducir</H3>
      <PlatformContainer>
        {this.getPlatforms(filmView.get("platformsSelected"))}
      </PlatformContainer>
    </div>
  );
  getSeasons = () => {
    const { filmView } = this.props;
    const seasons = Object.keys(filmView.get("episodes").toJS()).reduce(
      (acc, curr, key) => {
        const ep = filmView.getIn(["episodes", curr]);
        if (acc[curr]) {
          acc[curr] = ep.toArray();
        }
        acc[curr] = ep.toArray();
        return acc;
      },
      {}
    );

    return Object.keys(seasons).map(season => (
      <AccordionItem>
        <AccordionItemTitle>
          <h3>
            Temporada {season}{" "}
            <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjMycHgiIGhlaWdodD0iMzJweCIgdmlld0JveD0iMCAwIDMwNy4wNTQgMzA3LjA1NCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzA3LjA1NCAzMDcuMDU0OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPgoJPGcgaWQ9Il94MzRfODUuX1VwIj4KCQk8Zz4KCQkJPHBhdGggZD0iTTMwMi40NDUsMjA1Ljc4OEwxNjQuNjMsNjcuOTU5Yy02LjEzNi02LjEzLTE2LjA3NC02LjEzLTIyLjIwMywwTDQuNTk3LDIwNS43ODhjLTYuMTI5LDYuMTMyLTYuMTI5LDE2LjA2OSwwLDIyLjIwMSAgICAgbDExLjEwMSwxMS4xMDFjNi4xMjksNi4xMzYsMTYuMDc2LDYuMTM2LDIyLjIwOSwwbDExNS42Mi0xMTUuNjI2TDI2OS4xNTEsMjM5LjA5YzYuMTI4LDYuMTM2LDE2LjA3LDYuMTM2LDIyLjIwMSwwICAgICBsMTEuMTAxLTExLjEwMUMzMDguNTg5LDIyMS44NSwzMDguNTg5LDIxMS45MiwzMDIuNDQ1LDIwNS43ODh6IiBmaWxsPSIjMDAwMDAwIi8+CgkJPC9nPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" />
          </h3>
        </AccordionItemTitle>
        {seasons[season].map(episode => (
          <AccordionItemBody>
            <p>
              <strong>{episode.get("episode_number")}</strong>{" "}
              {episode.get("title")}
            </p>
          </AccordionItemBody>
        ))}
      </AccordionItem>
    ));
  };
  renderListButton = () => {
    const { user, filmView, onUserAction } = this.props;
    const remove =
      user.get("tosee") &&
      user.get("tosee").size > 0 &&
      user
        .get("tosee")
        .toArray()
        .filter(f => filmView.get("id") === f.get("id")).length > 0;
    return (
      <ButtonList
        onClick={() =>
          onUserAction(
            filmView.get("id"),
            remove ? "delete" : "add",
            this.props.user.get("email")
          )
        }
      >
        {remove ? (
          <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjkgMTI5IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAxMjkgMTI5IiB3aWR0aD0iMzJweCIgaGVpZ2h0PSIzMnB4Ij4KICA8Zz4KICAgIDxnPgogICAgICA8cGF0aCBkPSJtNjQuNSwxMjIuNGMzMS45LDAgNTcuOS0yNiA1Ny45LTU3LjlzLTI2LTU3LjktNTcuOS01Ny45LTU3LjksMjYtNTcuOSw1Ny45IDI2LDU3LjkgNTcuOSw1Ny45em0wLTEwNy43YzI3LjQtMy41NTI3MWUtMTUgNDkuOCwyMi4zIDQ5LjgsNDkuOHMtMjIuMyw0OS44LTQ5LjgsNDkuOC00OS44LTIyLjQtNDkuOC00OS44IDIyLjQtNDkuOCA0OS44LTQ5Ljh6IiBmaWxsPSIjMDAwMDAwIi8+CiAgICAgIDxwYXRoIGQ9Ik0zNy44LDY4aDUzLjNjMi4zLDAsNC4xLTEuOCw0LjEtNC4xcy0xLjgtNC4xLTQuMS00LjFIMzcuOGMtMi4zLDAtNC4xLDEuOC00LjEsNC4xUzM1LjYsNjgsMzcuOCw2OHoiIGZpbGw9IiMwMDAwMDAiLz4KICAgIDwvZz4KICA8L2c+Cjwvc3ZnPgo=" />
        ) : (
          <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxLjA1OSAzMS4wNTkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMxLjA1OSAzMS4wNTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0xNS41MjksMzEuMDU5QzYuOTY2LDMxLjA1OSwwLDI0LjA5MiwwLDE1LjUyOUMwLDYuOTY2LDYuOTY2LDAsMTUuNTI5LDAgICAgYzguNTYzLDAsMTUuNTI5LDYuOTY2LDE1LjUyOSwxNS41MjlDMzEuMDU5LDI0LjA5MiwyNC4wOTIsMzEuMDU5LDE1LjUyOSwzMS4wNTl6IE0xNS41MjksMS43NzQgICAgYy03LjU4NSwwLTEzLjc1NSw2LjE3MS0xMy43NTUsMTMuNzU1czYuMTcsMTMuNzU0LDEzLjc1NSwxMy43NTRjNy41ODQsMCwxMy43NTQtNi4xNywxMy43NTQtMTMuNzU0UzIzLjExMywxLjc3NCwxNS41MjksMS43NzR6IiBmaWxsPSIjMDAwMDAwIi8+Cgk8L2c+Cgk8Zz4KCQk8cGF0aCBkPSJNMjEuNjUyLDE2LjQxNkg5LjQwNmMtMC40OSwwLTAuODg4LTAuMzk2LTAuODg4LTAuODg3YzAtMC40OSwwLjM5Ny0wLjg4OCwwLjg4OC0wLjg4OGgxMi4yNDYgICAgYzAuNDksMCwwLjg4NywwLjM5OCwwLjg4NywwLjg4OEMyMi41MzksMTYuMDIsMjIuMTQzLDE2LjQxNiwyMS42NTIsMTYuNDE2eiIgZmlsbD0iIzAwMDAwMCIvPgoJPC9nPgoJPGc+CgkJPHBhdGggZD0iTTE1LjUyOSwyMi41MzljLTAuNDksMC0wLjg4OC0wLjM5Ny0wLjg4OC0wLjg4N1Y5LjQwNmMwLTAuNDksMC4zOTgtMC44ODgsMC44ODgtMC44ODggICAgYzAuNDksMCwwLjg4NywwLjM5OCwwLjg4NywwLjg4OHYxMi4yNDZDMTYuNDE2LDIyLjE0MywxNi4wMiwyMi41MzksMTUuNTI5LDIyLjUzOXoiIGZpbGw9IiMwMDAwMDAiLz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K" />
        )}
      </ButtonList>
    );
  };
  getView = () => {
    const {
      user,
      filmView,
      isWebView,
      onFilmSearch,
      filters,
      onFilterClick,
      onUserAction
    } = this.props;
    let view = "";

    if (this.props.filmView !== false) {
      view = (
        <ViewContainer
          playTrailer={this.state.showTrailer}
          isFilmView={this.props.filmView}
        >
          <Header
            goBack={this.props.goBack}
            onShowListClick={this.onShowListClick}
            showList={this.props.showList}
            user={user}
            dispatch={this.props.dispatch}
            search={this.props.search}
            onFilterClick={onFilterClick}
            filters={filters}
            onFilmSearch={onFilmSearch}
            filmView={filmView}
            onFilmClick={this.onFilmClickFilmView}
          />
          {false &&
          filmView.get("trailer") !== undefined &&
          filmView.get("trailer") !== "" &&
          this.state.showTrailer ? (
            <PlayContainerDiv play={this.state.showTrailer}>
              <SpanClose onClick={() => this.playTrailer(false)}>X</SpanClose>
              <LoadingIframe>
                <Iframe
                  title="trailer"
                  src={`https://www.youtube.com/embed/${filmView.get(
                    "trailer"
                  )}?autoplay=1`}
                  frameBorder="0"
                  allowFullScreen
                  className="hide"
                />
              </LoadingIframe>
            </PlayContainerDiv>
          ) : (
            ""
          )}
          {
          filmView.get("banner") !== undefined &&
          filmView.get("banner") !== "" ? (
            <TrailerContainer
              banner={filmView.get("banner")}
            />
          ) : (
            ""
          )}
          <TitleContainer>
            <H1 rel={filmView.get("id")}>{filmView.get("name")}</H1>
            <InfoYear>
              {filmView.get("year")} / <FilmGeneros film={filmView} />
            </InfoYear>
          </TitleContainer>
          {this.props.user.get("email") && this.renderListButton()}
          {false && (
            <button
              onClick={() =>
                onUserAction(
                  filmView.get("id"),
                  "seen",
                  "dani@gmail.com",
                  "sd",
                  "asdsd"
                )
              }
            >
              asdsdsadsa
            </button>
          )}
          {filmView.get("platformsSelected").size > 0 &&
            this.renderPlatforms(filmView)}
          <H3>Sinopsis</H3>
          <MainInfoContainer>
            <DivImg>
              <Img src={filmView.get("image")} alt="cover" />
            </DivImg>
            {this.getDescription(filmView.get("description"))}
          </MainInfoContainer>
          <H3>Notas</H3>
          <NoteContainer key={`${filmView.get("id")}_infofilm`}>
            {filmView.get("note_filmaffinity") !== undefined ? (
              <div>
                <img alt="star" src="/media/images/star.png" />{" "}
                <NoteInfo>
                  {parseFloat(filmView.get("note_filmaffinity"))}{" "}
                  <strong>Filmaffinity</strong>
                </NoteInfo>
              </div>
            ) : (
              ""
            )}
            {filmView.get("note_imdb") !== undefined ? (
              <div>
                <img alt="star" src="/media/images/star.png" />{" "}
                <NoteInfo>
                  {parseFloat(filmView.get("note_imdb"))} <strong>IMDB</strong>
                </NoteInfo>
              </div>
            ) : (
              ""
            )}
          </NoteContainer>
          {filmView.get("episodes") !== undefined &&
          filmView.get("episodes") !== "" ? (
            <div>
              <H3>Temporadas</H3>
              <DirectorContainer>
                <Accordion>{this.getSeasons()}</Accordion>
              </DirectorContainer>
            </div>
          ) : (
            ""
          )}
          {filmView.get("director") !== undefined &&
          filmView.get("director") !== "" ? (
            <div>
              <H3>Director</H3>
              <DirectorContainer>{filmView.get("director")}</DirectorContainer>
            </div>
          ) : (
            ""
          )}
          {filmView.get("actors") !== undefined &&
          filmView.get("actors").size > 0 ? (
            <div>
              <H3>Actores</H3>
              <ActoresContainer>
                {filmView.get("actors").map(a => <ActorItem>{a} </ActorItem>)}
              </ActoresContainer>
            </div>
          ) : (
            ""
          )}
          {filmView.get("filmaffinity") !== undefined &&
          filmView.get("filmaffinity").size > 0 ? (
            <div>
              <H3>Más información</H3>
              <MoreInfoContainer
                target="_blank"
                href={`https://www.filmaffinity.com${filmView.getIn([
                  "filmaffinity",
                  "0",
                  "url"
                ])}`}
              >
                Filmaffinity
              </MoreInfoContainer>
            </div>
          ) : (
            ""
          )}
        </ViewContainer>
      );
    } else {
      view = (
        <ViewContainer
          playTrailer={this.state.showTrailer}
          isFilmView={this.props.filmView}
        />
      );
    }
    return view;
  };
  render() {
    return this.getView();
  }
}

export default FilmView;
