import React from 'react';

const Loading = () => (
  <div style={{ textAlign: 'center' }}>
    <img src="/media/images/loading.gif" alt="loading" />
  </div>
);

export default Loading;
