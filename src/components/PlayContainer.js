import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { getPlatformId } from '../constants';

const PlayContainerDiv = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 10;
  background-color: rgba(0, 0, 0, 0.8)
`;

const LinkPlatform = styled.a`
display:inline-block
`;
const Container = styled.div`
position: relative;
top: 50%;
left: 50%;
transition: transform 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
transform:${props => (props.isReadMore === true ? 'translate(-50%, 500px);' : 'translate(-50%, -50%);')}
display:block;
width:${props => (props.numPlaforms > 1 ? '200px;' : '100px;')}
height:120px;
${props => (props.numPlaforms > 1 ? 'text-align:center;' : '')}
`;
const Img = styled.img`
display:block;
width:${props => (props.numPlaforms > 2 ? '50px;' : '')}
`;
const ReadMore = styled.span`
color:white;
cursor:pointer;
font-size:14px
`;
const ReadLess = styled.span`
color:white;
cursor:pointer;
float:right;
font-weight:bold;
padding:10px
`;
const PlayLess = styled.span`
color:white;
cursor:pointer;
float:right;
font-weight:bold;
padding:10px
`;
const ReadMoreContainer = styled.div`
transition: transform 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
overflow:auto;
height:100%;
transform:${props => (props.isReadMore === false ? 'translate(-50%, -500px);' : 'translate(4%, 0%);')}
position: relative;
color:white;
display:block;
width:95%;
::-webkit-scrollbar {
      width: 5px;
      background-color:#F5F5F5
};
::-webkit-scrollbar-track {
  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
border-radius: 10px;
background-color: #F5F5F5;
};
::-webkit-scrollbar-thumb {
  border-radius: 10px;
-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
background-color: black;
};
`;
const Hr = styled.hr`
border:0px;
height:10px;
width:100%
`;
const PlayContainer = ({
  film, idFilm, onShowMoreInfo, idFilmMore, onFilmClick, isWebView,
}) => {

  const isReadMore = film.get('id') === idFilmMore;
  const getPlatforms = platformsSelected => (
    platformsSelected.map(p => (
      <LinkPlatform key={`${p}_${film.get('id')}`} target="_blank" href={film.getIn(['platforms', getPlatformId(p)])}>
        <Img numPlaforms={platformsSelected.size} alt="logo" src={`/media/images/${p}.jpg`} />
      </LinkPlatform>
    ))
  );
  return (
    <div>
      {(true)
      ?
        <PlayContainerDiv >
          {(!isReadMore) ?
            <PlayLess onClick={() => onFilmClick('0')}>X<br /></PlayLess> : ''}
          {(!isReadMore) ?
            <Container isReadMore={isReadMore} numPlaforms={film.get('platformsSelected').size}>
              {getPlatforms(film.get('platformsSelected'))}
              {(film.get('description') !== '') ? <ReadMore onClick={() => onShowMoreInfo(film.get('id'))} ><br />Leer sinopsis</ReadMore> : ''}
            </Container>
          : ''}
          {
            (film.get('description') !== undefined) ?
              <ReadMoreContainer isReadMore={isReadMore} >
                <ReadLess onClick={() => onShowMoreInfo('0')}>X</ReadLess>
                <Hr />
                {film.get('description')}
                <br />
              </ReadMoreContainer>
          : ''
        }
        </PlayContainerDiv>
      :
        <PlayContainerDiv >
          <ReadMoreContainer >
            <ReadLess onClick={() => onFilmClick('0')}>X</ReadLess>
            <Hr />
            {film.get('description')}
            <br />
          </ReadMoreContainer>
        </PlayContainerDiv >
    }
    </div>
  );
};

PlayContainer.propTypes = {
  film: PropTypes.object.isRequired,
  onShowMoreInfo: PropTypes.func.isRequired,
  onFilmClick: PropTypes.func.isRequired,
  idFilm: PropTypes.string.isRequired,
  idFilmMore: PropTypes.string.isRequired,
};
export default PlayContainer;
