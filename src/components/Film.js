import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import PlayContainer from './PlayContainer';
import FilmGeneros from './FilmGeneros';

const ImgContainer = styled.div`
display: inline-block;
  background: #fff;
  margin: 0 0 1.5em;
  width: 100%;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-shadow: 2px 2px 4px 0 #ccc;
  position:relative;
  overflow:hidden;
  cursor:pointer
`;
const Filmaffinity = styled.div`
  vertical-align:top;
  display:inline-block;
  margin-left: 5px;
  width:57%;
  display:inline-block;
`;
const Center = styled.div`
  display:inline-block;
  width:40%;
  display:inline-block
`;
const NoteContainer = styled.div`
  margin-bottom:5px;
  font-size:14px;
  line-height:30px;
  margin-top:10px
`;
const Img = styled.img`
width:100%;
vertical-align:bottom
`;

const InfoYear = styled.span`
 font-size:12px;
 color:#AAAAAA
`;
const A = styled.span`
text-decoration: none;
  color: white;
  background: #4682B4;
  padding: 5px;
  border-radius: 5px;
`;
const H3 = styled.h3`
  font-size:18px;
  margin-bottom:0px;
  overflow: hidden;
  color:#444;
   text-overflow: ellipsis;
   display: -webkit-box;
   line-height: 20px;     /* fallback */
   max-height: 40px;      /* fallback */
   -webkit-line-clamp: 2; /* number of lines to show */
   -webkit-box-orient: vertical;
   margin-right:30px
`;
const ImgPlatform = styled.img`
  width:35px;
  box-shadow:rgb(204, 204, 204) 2px 2px 4px 0px;
`;
const PlatFormcontainer = styled.div`
  text-align:right;
  position:absolute;
  width:30px;
  right:5px
`;

const NoteInfo = styled.div`
  line-height:20px;
  display:inline-block;
  color:#444
`;

const Film = ({
  film, onFilmClick, idFilm, onShowMoreInfo, idFilmMore, isWebView,
}) => {
  const getInfoFilm = () => {
    let infoContainer = '';
    infoContainer =
        (<div><NoteContainer key={`${film.get('id')}_infofilm`}>
          {(film.get('note_filmaffinity') !== undefined) ?
            <div>
              <img alt="star" src="/media/images/star.png" /> <NoteInfo>{parseFloat(film.get('note_filmaffinity'))} <strong>Filmaffinity</strong></NoteInfo>
            </div>
          : ''}
          {(film.get('note_imdb') !== undefined) ?
            <div>
              <img alt="star" src="/media/images/star.png" /> <NoteInfo>{parseFloat(film.get('note_imdb'))} <strong>IMDB</strong></NoteInfo>
            </div>
            : ''}
        </NoteContainer>
          <FilmGeneros film={film} isFilmList />
        </div>
        );
    return infoContainer;
  };
  const getPlatforms = platformsSelected => (
    platformsSelected.map(p => (
      <ImgPlatform key={`${p}.jpg`} alt="plaform" src={`/media/images/${p}.jpg`} />
    ))
  );
  return (
    <ImgContainer id={film.get('id')}>
      {(film.get('id') === idFilm) ? <PlayContainer isWebView={isWebView} onFilmClick={onFilmClick} film={film} idFilm={idFilm} idFilmMore={idFilmMore} onShowMoreInfo={onShowMoreInfo} /> : ''}
      <div onClick={() => onFilmClick(film.get('id'))}>
        <Center>
          <Img src={film.get('image')} alt="caratula" />
        </Center>
        <Filmaffinity>
          <PlatFormcontainer>{getPlatforms(film.get('platformsSelected'))}</PlatFormcontainer>
          <H3>{film.get('name')}</H3>
          <InfoYear>{film.get('year')}</InfoYear>
          {getInfoFilm()}

        </Filmaffinity>
      </div>
    </ImgContainer>
  );
};

Film.propTypes = {
  film: PropTypes.object.isRequired,
  onFilmClick: PropTypes.func.isRequired,
  onShowMoreInfo: PropTypes.func.isRequired,
  idFilm: PropTypes.string.isRequired,
  idFilmMore: PropTypes.string.isRequired,
};
export default Film;
