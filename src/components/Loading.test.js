import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import Loading from './Loading';

describe('Loading test', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(
    <Loading />,
  );
  test('should render correctly', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
  test('render the Loading component', () => {
    expect(wrapper.length).toEqual(1);
  });
  test('Should have only one img', () => {
    expect(wrapper.find('img').length).toEqual(1);
  });
});
