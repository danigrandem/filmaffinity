import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { generosArray, generos } from '../constants';

const GeneroItem = styled.div`
   display:inline-block;
   position:relative;
   ${props => (props.isFilmList === true ? 'color:rgb(68, 68, 68);' : 'color:#AAAAAA;')}
   text-transform: capitalize;
   ${props => (props.isFilmList === true ? 'font-size:14px;' : '')}

`;
const FilmGeneros = ({
  film, isFilmList,
}) => {
  const getGeneros = () => {
    const generosReturn = [];
    generosArray.map((g) => {
      if (film.get(`genero_${g}`) !== undefined) {
        generosReturn.push(generos[g]);
      }
    });

    return generosReturn.map((gen, key) => <GeneroItem isFilmList={isFilmList}>{gen}{(key < generosReturn.length - 1) ? ', ' : ''}</GeneroItem>);
  };
  return (
    getGeneros()
  );
};

export default FilmGeneros;
