import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { generos, generosArray, platforms } from '../constants';
import RangeSelector from '../components/RangeSelector';
import FilterPerYear from './FilterPerYear';
import { Link } from 'react-router-dom';
import FilterPerNote from './FilterPerNote';
import './FiltersModal.css';

const Modal = styled.div`
top: 0;
position: fixed;
width: 100%;
height: 100%;
z-index:9999;
color:#484848;
background: white;
right:0px;
overflow:auto;
overflow-x:hidden;
transition: transform 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
transform:${props => (props.showFilterModal === true ? 'translate(0px, 0px);' : 'translate(2500px, 0px);')}
@media (min-width: 800px) {
  width:400px;
  background:white;
  box-shadow:-3px 0 14px 0 rgba(50,50,50,.45);
};
`;

const RangeContainer = styled.div`
  padding:15px;
  display:block
`;
const FilterMenuTitleContainer = styled.div`
border-bottom:1px solid #DBDBDB;
  padding: 12px 15px;
  text-transform: uppercase;
  font-size: 14px;
`;
const Ul = styled.div`
  padding-left:0px;
  position:relative;
  ${props => (props.margin !== undefined ? 'margin-top:10px' : '')}
`;
const Li = styled.div`
  margin-top:10px;
  color:#484848;
  padding-left:20px;
`;
const FilterMenuTitle = styled.strong`
  font-weight:bold;
  font-size:20px;
  color:#484848;
  padding-left:10px;

`;
const FilterLink = styled.button`
  font-size:16px;
  color:#484848;
  text-decoration:none;
  background:none;
  border:none;
  width:100%;
  text-align:left;
  cursor:pointer;
  text-transform: capitalize
`;
const HeaderModal = styled.div`
  padding:10px;
  border-bottom: 1px solid #DBDBDB;
  width:100%;
  font-size:24px;
  display: -webkit-box;
display: -webkit-flex;
display: -ms-flexbox;
display: flex;
-webkit-box-pack: justify;
-webkit-justify-content: space-between;
-ms-flex-pack: justify;
justify-content: space-between;
-webkit-align-items: center;
-webkit-box-align: center;
-ms-flex-align: center;
align-items: center;
  width: auto;
`;
const Button = styled.button`
  border:none;
  background:none;
  font-weight:bold;
  cursor:pointer
`;
const SpanCheck = styled.span`
  position:absolute;
  right:20px
`;

const ButtonSave = styled.button`
  -webkit-transition: background-color 0.25s ease;
  transition: background-color 0.25s ease;
  padding: 0 1.4rem;
  min-height: 30px;
  border-radius: 100em;
  background-color: #3F51B5;
  color: white;
  width: auto;
  height: 48px;
  font-size: 16px;
  border:0px;
  position:relative;
  overflow:hidden;

  &:active {
    outline: 0;
    background: #7c8adb;
  }
`;
const DivSave = styled.div`
  position: relative;
  bottom: 0;
  left: 0;
  right: 0;
  text-align: center;
`;

const DivPadding = styled.div`
   padding-bottom:24px
`;
class FiltersModal extends Component {
  setInitialStateObject = (arr) => {
    const obj = {};
    arr.map((a) => {
      obj[a] = true;
    });
    return obj;
  }
  constructor(props, context) {
    super(props, context);

    this.state = {
      note: this.setInitialStateObject(props.filters.get('note').split(';')),
      genero: this.setInitialStateObject(props.filters.get('genero').split(';')),
      platform: this.setInitialStateObject(props.filters.get('platform').split(';')),
      year: this.setInitialStateObject(props.filters.get('year').split(';')),
      showType: this.setInitialStateObject(props.filters.get('showType').split(';')),
      showButton: false,
    };
  }
  generoFilters = () => (
    generosArray.map((g, i) => (
      <Li key={`${this.state.genero}_${g}`}>
        <Link onClick={e => e.preventDefault()} to={`/${this.props.setUrl(g, this.getStates(this.state.platform).join(';'), this.getStates(this.state.note).join(';'), this.getStates(this.state.year).join(';'), this.getStates(this.state.showType).join(';'))}`} >
          <FilterLink onClick={() => this.filterClickObjects('genero', g)}>
            {generos[g]}
          </FilterLink>
        </Link>{(this.state.genero[g] === true) ?
          <SpanCheck><img src="/media/ok.png" alt="filters" /></SpanCheck> :
          ''}
      </Li>
    ))
  );
  platformsFilters = () => (
    platforms.map((p, i) => (
      <Li key={`${this.state.platform}_${p}`}>
        <Link onClick={e => e.preventDefault()} to={`/${this.props.setUrl(this.getStates(this.state.genero).join(';'), p, this.getStates(this.state.note).join(';'), this.getStates(this.state.year).join(';'), this.getStates(this.state.showType).join(';'))}`} >
          <FilterLink onClick={() => this.filterClickObjects('platform', p)}>
            {p === 'yomvi'? 'Movistar + ': p}
          </FilterLink>
        </Link>{(this.state.platform[p] === true)
          ? <SpanCheck><img src="/media/ok.png" alt="filters" /></SpanCheck>
          : ''}
      </Li>
    ))
  );
  filterClick = (year = 'all', showType = 'all') => {
    this.setState({
      year,
      showButton: true,
    });
  }
  setStateObject = (campo, value) => {
    const obj = this.state[campo];
    if (obj[value] === true) {
      obj[value] = false;
    } else {
      obj[value] = true;
    }
    if (value !== 'all') {
      let isAll = true;
      Object.keys(obj).map((key) => {
        if (obj[key] === true) {
          isAll = false;
        }
      });
      obj.all = isAll;
    } else {
      Object.keys(obj).map((key) => {
        obj[key] = false;
      });
      obj.all = true;
      return obj;
    }
    return obj;
  }
  filterClickObjects = (campo, value) => {
    const obj = this.setStateObject(campo, value);
    this.setState(obj);
  }
  getStates = (obj) => {
    const arr = [];
    Object.keys(obj).map((key) => {
      if (obj[key] === true) {
        arr.push(key);
      }
    });
    return arr;
  }
  onSendSubmit = () => {
    this.props.onFilterClick(
      this.getStates(this.state.genero).join(';'),
      this.getStates(this.state.platform).join(';'),
      this.getStates(this.state.note).join(';'),
      this.getStates(this.state.year).join(';'),
      this.getStates(this.state.showType).join(';'),
    );
    this.setState({
      showButton: false,
    });
  }
  handleNoteChange = (e) => {
    this.setState({
      note: (e.target.value !== '0') ? e.target.value : 'all',
      showButton: true,
    });

    // onFilterClick(genero, platform, e.target.value);
  }
  onRestoreClick = (e) => {
    this.setState({
      note: { all: true },
      genero: { all: true },
      platform: { all: true },
      year: { all: true },
      showType: { all: true },
      showButton: true,
    });
    // onFilterClick(genero, platform, e.target.value);
  }
  render() {
    const {
      filters, onFilterModalClick, onFilterClick, showFilterModal, onRangeChange, setUrl,
    } = this.props;
    const { note } = this.state;
    return (
      <div>
        <Modal showFilterModal={showFilterModal}>
          <HeaderModal>
            <Button onClick={() => onFilterModalClick(false)} >Cancelar</Button>
            <span>Filtros</span>
            <Button onClick={() => this.onRestoreClick()} >Borrar</Button>
          </HeaderModal>
          <Ul>
            <FilterMenuTitleContainer><FilterMenuTitle>Por Tipo</FilterMenuTitle></FilterMenuTitleContainer>
            <Li>
              <FilterLink href="#body" onClick={() => this.filterClickObjects('showType', 'all')}>
                Todos
              </FilterLink>{(this.state.showType.all === true) ? <SpanCheck><img src="/media/ok.png" alt="filters" /></SpanCheck> : ''}
            </Li>
            <Li >
              <Link onClick={e => e.preventDefault()} to={`/${this.props.setUrl(this.getStates(this.state.genero).join(';'), this.getStates(this.state.platform).join(';'), this.getStates(this.state.note).join(';'), this.getStates(this.state.year).join(';'), 'movie')}`} >
                <FilterLink onClick={() => this.filterClickObjects('showType', 'movie')}>
                Películas
                </FilterLink>
              </Link>{(this.state.showType.movie === true)
                ? <SpanCheck><img src="/media/ok.png" alt="filters" /></SpanCheck>
                : ''}
            </Li>
            <Li >
              <Link onClick={e => e.preventDefault()} to={`/${this.props.setUrl(this.getStates(this.state.genero).join(';'), this.getStates(this.state.platform).join(';'), this.getStates(this.state.note).join(';'), this.getStates(this.state.year).join(';'), 'show')}`} >
                <FilterLink onClick={() => this.filterClickObjects('showType', 'show')}>
                Series
                </FilterLink>
              </Link>{(this.state.showType.show === true)
                ? <SpanCheck><img src="/media/ok.png" alt="filters" /></SpanCheck>
                : ''}
            </Li>
          </Ul>
          <Ul margin={10}>
            <FilterMenuTitleContainer><FilterMenuTitle>Por plataforma</FilterMenuTitle></FilterMenuTitleContainer>
            <Li>
              <FilterLink href="#body" onClick={() => this.filterClickObjects('platform', 'all')}>
                Todos
              </FilterLink>{(this.state.platform.all === true) ? <SpanCheck><img src="/media/ok.png" alt="filters" /></SpanCheck> : ''}
            </Li>
            {this.platformsFilters()}
          </Ul>
          <Ul margin={10}>
            <FilterMenuTitleContainer><FilterMenuTitle>Por Nota</FilterMenuTitle></FilterMenuTitleContainer>
            <FilterPerNote filterClick={this.filterClickObjects} note={this.state.note} showType={this.state.showType} year={this.state.year} genero={this.state.genero} platform={this.state.platform} />
          </Ul>
          <Ul margin={10}>
            <FilterMenuTitleContainer><FilterMenuTitle>Por Año</FilterMenuTitle></FilterMenuTitleContainer>
            <FilterPerYear filterClick={this.filterClickObjects} showType={this.state.showType} year={this.state.year} genero={this.state.genero} platform={this.state.platform} />
          </Ul>
          <Ul>
            <FilterMenuTitleContainer><FilterMenuTitle>Por género</FilterMenuTitle></FilterMenuTitleContainer>
            <Li>
              <FilterLink href="#body" onClick={() => this.filterClickObjects('genero', 'all')}>
                Todos
              </FilterLink>{(this.state.genero.all === true) ? <SpanCheck><img src="/media/ok.png" alt="filters" /></SpanCheck> : ''}
            </Li>
            {this.generoFilters()}
          </Ul>
          {(true)
            ? <DivSave>
              <DivPadding>
                <ButtonSave onClick={this.onSendSubmit} >Guardar filtros</ButtonSave>
              </DivPadding>
            </DivSave>
            :
              ''}

        </Modal>
      </div>
    );
  }
}

FiltersModal.propTypes = {
  onFilterModalClick: PropTypes.func.isRequired,
  filters: PropTypes.object.isRequired,
  onRangeChange: PropTypes.func.isRequired,
  onFilterClick: PropTypes.func.isRequired,
  showFilterModal: PropTypes.bool.isRequired,
};
export default FiltersModal;
