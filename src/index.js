import React from 'react';
import { fromJS } from 'immutable';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import './index.css';
import { routes } from './routes';
import reducer from './reducers/index';
import mySaga from './sagas';


const sagaMiddleware = createSagaMiddleware();
// mount it on the Store

const preloadedState = window.__PRELOADED_STATE__;
delete window.__PRELOADED_STATE__;
const store = createStore(reducer, fromJS(preloadedState), applyMiddleware(sagaMiddleware));

// then run the saga
sagaMiddleware.run(mySaga);
ReactDOM.render(
  <Provider store={store}>
    <Router>
      {routes}
    </Router>
  </Provider>, document.getElementById('root'));
