import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import './App.css';
import FilmList from '../components/FilmList';
import FilmView from '../components/FilmView';
import FiltersModal from '../components/FiltersModal';
import { perPage } from '../constants';
import Loading from '../components/Loading';
import InitialLoading from '../components/InitiaLoading';
import Header from '../components/Header';
import Auth from '../services/Auth';
import User from '../components/User/User';
import scrollTo from "../scrollTo";

const H1 = styled.h1`
  display:inline;
  font-size:22px;
  color:#444;

`;
const FilterLink = styled.button`
  float:right;
  background:none;
  border:0px;
  cursor:pointer;
  position:fixed;
  background-color:#3F51B5;
  bottom: 10px;
  border-radius: 30px;
  padding: 15px;
  right: 15px;
  box-shadow:rgb(204, 204, 204) 2px 2px 4px 0px;


  &:active {
    outline: 0;
    background: #7c8adb;
  }
`;
const Container = styled.div`
  padding:10px
`;

const ImgFilter = styled.img`
  width:15px
`;

const ErrorMsg = styled.div`
  padding:10px;
  background:red;
  color:white;
  margin-top:20px
`;


const DivHeader = styled.div`
  margin-bottom:20px
`;

const ImgHide = styled.img`
  display:none
`;
class App extends Component {
  componentDidMount() {
    const { filters, isSsr } = this.props;
    if (!isSsr) {
      const { idFicha } = this.props.match.params;
      if (idFicha !== undefined) {
        this.props.dispatch({
          type: 'FILM_FETCH_REQUEST',
          idFicha,
        });
      } else {
        this.props.dispatch({
          type: 'USER_FETCH_REQUESTED',
          platform: (this.props.match.params.plataforma !== undefined) ? this.props.match.params.plataforma : filters.get('platform'),
          note: (this.props.match.params.nota !== undefined) ? this.props.match.params.nota : filters.get('note'),
          genero: (this.props.match.params.genero !== undefined) ? this.props.match.params.genero : filters.get('genero'),
          year: (this.props.match.params.year !== undefined) ? this.props.match.params.year : filters.get('year'),
          showType: (this.props.match.params.show !== undefined) ? this.props.match.params.show : 'all',
          order: filters.get('order'),
          limit: perPage,
        });
      }
    }
    window.addEventListener('mousedown', this.handleClick, false);
    window.addEventListener('scroll', this.onScroll, false);

    this.props.history.listen((location, action) => {
      if (action === 'POP') {
        const { idFicha } = this.props.match.params;
        if (idFicha !== undefined) {
          this.props.dispatch({
            type: 'FILM_FETCH_REQUEST',
            idFicha,
          });
        } else {
          const platform = (this.props.match.params.plataforma !== undefined) ? this.props.match.params.plataforma : 'all';
          const note = (this.props.match.params.nota !== undefined) ? this.props.match.params.nota : 'all';
          const genero = (this.props.match.params.genero !== undefined) ? this.props.match.params.genero : 'all';
          const year = (this.props.match.params.year !== undefined) ? this.props.match.params.year : 'all';
          const showType = (this.props.match.params.show !== undefined) ? this.props.match.params.show : 'all';
          const order = filters.get('order');
          this.getListCheck(platform, note, genero, year, showType, order);
        }
      }
    });
  }
  componentDidUnMount() {
    window.removeEventListener('mousedown', this.handleClick, false);
    window.removeEventListener('scroll', this.onScroll, false);
  }
  handleClick = (e) => {
    if (this.node !== null && this.node.contains(e.target)) {
      return;
    }
    this.onFilterModalClick(false);
  }
  onScroll = () => {
    const {
      inProgress, showFilterModal, filters, films, hasMore, showList
    } = this.props;

    const list = (films.size > 0) ? films.getIn([filters.get('showType'), filters.get('genero'), filters.get('platform'), filters.get('note'), filters.get('year'), filters.get('order')]) : [];
    if (!showList && showFilterModal !== true &&
     (window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 700) &&
     list.size > 0 && !inProgress && hasMore.getIn([filters.get('showType'), filters.get('genero'), filters.get('platform'), filters.get('note'), filters.get('year'), filters.get('order')])
    ) {
      this.props.dispatch({
        type: 'USER_PAGINATE_PROGRESS', inProgress: true,
      });
      this.props.dispatch({
        type: 'USER_FETCH_REQUESTED',
        platform: filters.get('platform'),
        note: filters.get('note'),
        genero: filters.get('genero'),
        year: filters.get('year'),
        showType: filters.get('showType'),
        limit: list.size + perPage,
        order: filters.get('order'),
      });
    }
  }
  setUrl = (genero, platform, note, year, showType) => {
    const url = [];
    if (showType !== 'all') {
      url.push(`tipo/${showType}`);
    }
    if (platform !== 'all') {
      url.push(`plataforma/${platform}`);
    }
    if (note !== 'all') {
      url.push(`nota/${note}`);
    }
    if (genero !== 'all') {
      url.push(`genero/${genero}`);
    }
    if (year !== 'all') {
      url.push(`year/${year}`);
    }
    return url.join('/');
  }
  getListCheck = (platform, note, genero, year, showType, order, limit = perPage) => {
    if (this.props.films.getIn([showType, genero, platform, note, year, order]) === undefined) {
      const showFilterModal = false;
      // this.props.dispatch({ type: 'FILTER_IN_PROGRESS', changeFilter: true });
      // this.props.dispatch({ type: 'SHOW_FILTER_MODAL', showFilterModal });
      this.props.dispatch({
        type: 'USER_FETCH_REQUESTED', platform, note, genero, page: 1, year, showType, order, changeFilter: true, showFilterModal, limit
      });
    } else {
      this.props.dispatch({
        type: 'SET_FILTERS_FETCH', platform, note, genero, page: 1, year, showType, order,
      });
    }
    if (document !== undefined) {
      document.getElementById('body').className = '';
    }
  }
  onFilterClick = (genero, platform, note, year, showType, order = this.props.filters.get('order')) => {
    this.getListCheck(platform, note, genero, year, showType, order);
scrollTo(0, 0);
    this.props.history.push(`/${this.setUrl(genero, platform, note, year, showType)}`);
  }
  goBack = (genero, platform, note, year, showType, order = this.props.filters.get('order')) => {
    this.getListCheck(platform, note, genero, year, showType, order);
    this.props.history.push(`/${this.setUrl(genero, platform, note, year, showType)}`);
  }
  onFilterModalClick = (showFilterModal) => {
    this.props.dispatch({ type: 'SHOW_FILTER_MODAL', showFilterModal });
    if (document.getElementById('body') !== null && this.props.filmView === false) {
      document.getElementById('body').className = (showFilterModal) ? 'noOverFlow' : '';
    }

    //this.props.history.push('#menu');
  }
  onLoaded = () => {
    this.props.dispatch({ type: 'HIDE_LOADING', isSsr: false });
  }
  onOrderChange = (order) => {
    const {
      filters,
    } = this.props;
    this.onFilterClick(filters.get('genero'), filters.get('platform'), filters.get('note'), filters.get('year'), filters.get('showType'), order);
  }
  getLoading = (inProgress, hasMoreFilter, changeFilter) => (
    ((inProgress && hasMoreFilter) || changeFilter) ? <Loading /> : ''
  )
  onRangeChange = (e) => {
    console.log('ola', e.target.value);
  }
  onFilmClick = (idFilm) => {
    if (document.getElementById('body') !== null) {
      document.getElementById('body').className = (idFilm !== false) ? 'noOverFlow' : '';
    }
    this.props.dispatch({ type: 'ITEM_VIEW', idFilm });

    //this.props.history.push(`/ficha/${idFilm}`);
  }
  onUserAction = (idFilm, actionType, email) => {
    console.log("call");
    this.props.dispatch({ type: 'ON_USER_ACTION', idFilm, actionType, email });
  }
  onFilmSearch = (film) => {
    if (document.getElementById('body') !== null) {
      document.getElementById('body').className = (film !== false) ? 'noOverFlow' : '';
    }
    this.props.dispatch({ type: 'ITEM_VIEW_SEARCH', film });
    this.props.history.push(`/ficha/${film.id}`);
  }
  onShowMoreInfo = (idFilmMore) => {
    this.props.dispatch({ type: 'SHOW_MORE_INFO', idFilmMore });
  }
  getTitle = (filters) => {
    if (this.props.filmView !== false) return this.props.filmView.get('name');
    let title = 'Las mejores peliculas y series';

    if (this.props.isWebView) {
      return title;
    }
    title = 'Las mejores ';
    if (filters.get('showType') !== 'all') {
      const type = filters.get('showType');
      title = title.concat(` ${type.replace(/;/g, ', ').replace('movie', 'películas').replace('show', 'series')}`);
    } else {
      title = title.concat(' películas y series');
    }
    if (filters.get('platform') !== 'all') {
      title = title.concat(` para ver en ${filters.get('platform').replace(/;/g, ', ')}`);
    }
    if (filters.get('genero') !== 'all') {
      title = title.concat(` de ${filters.get('genero').replace(/;/g, ', ')}`);
    }
    if (filters.get('note') !== 'all') {
      title = title.concat(` con nota ${filters.get('note').replace(/;/g, ', ')}`);
    }
    if (filters.get('year') !== 'all') {
      title = title.concat(` del año ${filters.get('year').replace(/;/g, ', ')}`);
    }
    return title.concat(' según filmaffinity');
  }
  getList = (list, changeFilter, isWebView, idFilm, idFilmMore) => {
    let listReturn = '';
    if (list !== undefined && list.size > 0 && !changeFilter) {
      listReturn = <FilmList showList={this.props.showList} filters={this.props.filters} isWebView={isWebView} films={list} onFilmClick={this.onFilmClick} idFilm={idFilm} idFilmMore={idFilmMore} onShowMoreInfo={this.onShowMoreInfo} />;
    } else if (!changeFilter && !this.props.showList) {
      listReturn = <ErrorMsg>No hay películas con el filtro seleccionado</ErrorMsg>;
    } else if(this.props.showList) {
      listReturn = <ErrorMsg>No tienes ningún título añadido</ErrorMsg>;
    }
    return listReturn;
  }
  login = () => {
    const auth = new Auth();
    auth.login();
  }

  getListItems = () => {
    const {films,filters} =this.props;
    if(this.props.showList){
      return this.props.user.get("tosee");
    }
return (films.size > 0) ? films.getIn([filters.get('showType'), filters.get('genero'), filters.get('platform'), filters.get('note'), filters.get('year'), filters.get('order')]) : [];
  }

  getView = (list, changeFilter, isWebView, idFilm, idFilmMore, title, inProgress, hasMoreFilter, showFilterModal, filters, filmView) =>
    (
      <div>
        {(!isWebView) ?
          <DivHeader>
            <H1>{title}</H1>
          </DivHeader>
        :
        ''
      }
        {this.getList(list, changeFilter, isWebView, idFilm, idFilmMore)}
        {this.getLoading(inProgress, hasMoreFilter, changeFilter)}
        <div ref={(node) => { this.node = node; return this.node; }}>
          <FiltersModal setUrl={this.setUrl} onRangeChange={this.onRangeChange} showFilterModal={showFilterModal} filters={filters} onFilterModalClick={this.onFilterModalClick} onFilterClick={this.onFilterClick} />
        </div>
        <FilterLink onClick={() => {scrollTo(0,0)}}><ImgFilter src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMS4xLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDg2LjAwMiA4Ni4wMDIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDg2LjAwMiA4Ni4wMDI7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iMzJweCIgaGVpZ2h0PSIzMnB4Ij4KPGc+Cgk8cGF0aCBkPSJNODAuMDkzLDY0Ljk5OWMxLjM1MywxLjMzOCwzLjU0NCwxLjMzOCw0Ljg5NiwwYzEuMzUtMS4zMzgsMS4zNTItMy41MDYsMC00Ljg0N0w0NS40NDUsMjEuMDA0ICAgYy0xLjM1Mi0xLjMzOC0zLjU0MS0xLjMzOC00Ljg5MiwwTDEuMDEzLDYwLjE1MmMtMS4zNSwxLjM0MS0xLjM1MiwzLjUwNywwLDQuODQ3YzEuMzUyLDEuMzM4LDMuNTQzLDEuMzM4LDQuODk1LDBsMzcuMDkxLTM1LjcwMyAgIEw4MC4wOTMsNjQuOTk5eiIgZmlsbD0iI0ZGRkZGRiIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" alt="arriba" /></FilterLink>
        <FilmView goBack={this.goBack} showList={this.props.showList} user={this.props.user} dispatch={this.props.dispatch} search={this.props.search} onUserAction={this.onUserAction} onFilmSearch={this.onFilmSearch} onFilterClick={this.onFilterClick} isWebView={isWebView} filters={filters} filmView={filmView} onFilmClick={this.onFilmClick} />
      </div>)
  render() {
    const {
      films, user,search, filters, showFilterModal, inProgress, isSsr, dispatch, hasMore, idFilm, idFilmMore, isWebView, changeFilter, filmView,
    } = this.props;
    const list = this.getListItems();
    const hasMoreFilter = hasMore.getIn([filters.get('showType'), filters.get('genero'), filters.get('platform'), filters.get('note'), filters.get('year'), filters.get('order')]);
    const title = this.getTitle(filters);
    return (
      <div>
        {!filmView && <Header goBack={this.goBack} history={this.props.history} onFilterModalClick={this.onFilterModalClick}  showList={this.props.showList} user={this.props.user} dispatch={this.props.dispatch} search={this.props.search} onFilterClick={this.onFilterClick} onFilmSearch={this.onFilmSearch} filmView={filmView} filters={filters} onOrderChange={this.onOrderChange} onFilmClick={this.onFilmClick} />}
        <Container>
          <ImgHide src="/media/images/netflix.jpg" />
          <ImgHide src="/media/images/hbo.jpg" />
          <ImgHide src="/media/images/yomvi.jpg" />
          <ImgHide src="/media/images/amazon.jpg" />
          <ImgHide src="/media/images/filmin.jpg" />
          <ImgHide src="/media/images/rakuten.jpg" />
          <ImgHide src="/media/images/back.png" />
          {(false) ? <InitialLoading dispatch={dispatch} onLoaded={this.onLoaded} /> : ''}
          <Helmet>
            <meta charSet="utf-8" />
            <title>{(this.props.filmView === false) ? title : `Ver ${title} online`}</title>
          </Helmet>
          {this.getView(list, changeFilter, isWebView, idFilm, idFilmMore, title, inProgress, hasMoreFilter, showFilterModal, filters, filmView)}
          {false && <User email="dani@gmail.com" fullName="Dani" avatar="https://scontent-mad1-1.xx.fbcdn.net/v/t1.0-1/p24x24/15941108_10154768054611093_2629943035499466052_n.jpg?_nc_cat=0&oh=2343e79da262c319437d5b2431d28472&oe=5BBDCB5D" />}
        </Container>
      </div>
    );
  }
}
App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  changeFilter: PropTypes.any.isRequired,
  filters: PropTypes.object.isRequired,
  films: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  match: PropTypes.any.isRequired,
  filmView: PropTypes.any.isRequired,
  idFilm: PropTypes.string.isRequired,
  idFilmMore: PropTypes.string.isRequired,
  inProgress: PropTypes.bool.isRequired,
  isSsr: PropTypes.bool.isRequired,
  isWebView: PropTypes.bool.isRequired,
  showFilterModal: PropTypes.bool.isRequired,
  hasMore: PropTypes.any.isRequired,
};
const mapStateToProps = state => (
  {
    films: state.get('films'),
    user:state.get('user'),
    showList:state.get('showList'),
    search: state.get('search'),
    filters: state.get('filters'),
    user: state.get('user'),
    hasMore: state.get('hasMore'),
    inProgress: state.get('inProgress'),
    isWebView: state.get('isWebView'),
    showFilterModal: state.get('showFilterModal'),
    isSsr: state.get('isSsr'),
    idFilm: state.get('idFilm'),
    idFilmMore: state.get('idFilmMore'),
    changeFilter: state.get('changeFilter'),
    filmView: state.get('filmView'),
  }
);
const mapDispatchToProps = dispatch => (
  {
    dispatch,
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(App);
