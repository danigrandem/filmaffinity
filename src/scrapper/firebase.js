const admin = require('firebase-admin');
const algoliasearch = require('algoliasearch');
const serviceAccount = require('./yomvi-f72170ddf42f.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://yomvi-b2179.firebaseio.com/',
});
const db = admin.firestore();
const database = admin.database();
module.exports.db = db;
module.exports.database = database;

// configure algolia
const algolia = algoliasearch(
  'A93UOXNN0S',
  'f0ce541046925964e77801cc93e4a20f'
);
const index = algolia.initIndex('films');
module.exports.index = index;
module.exports.algolia = algolia;
