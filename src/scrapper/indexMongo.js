const bs = require("nodestalker");

const axios = require("axios");
const cheerio = require("cheerio");
const request = require("request");
const slugify = require("slugify");
const {
  baseFilmaffinityUrl,
  baseRakutenUrl,
  baseYomviUrlNew,
  baseYomviUrl,
  baseNetflixUrl,
  baseHBOUrl,
  baseFilminUrl,
  baseAMZUrl,
  mongoUrl,
  mongoCollection,
  getPlatformName,
  genreMapping
} = require("../constants.js");
const { Proxy } = require("./Proxy.js");

let url = "";
let urlNetflix = "";
let urlHBO = "";
let urlAMZ = "";
let urlFilmin = "";
let urlRakuten = "";
let genero = "";
if (process.argv.length === 2) {
  console.log(
    "Envia un genero: accion, animacion, aventura, scfi, independiente, infantil, drama, suspense, terror ..."
  );
  process.exit();
} else {
  genero = process.argv[2];
  url = baseYomviUrlNew[genero];
  urlNetflix = baseNetflixUrl[genero];
  urlHBO = baseHBOUrl[genero];
  urlAMZ = baseAMZUrl[genero];
  urlFilmin = baseFilminUrl[genero];
  urlRakuten = baseRakutenUrl[genero];
}
console.log("GENERO", genero);
const films = [];

const enqueue = elem => {
  const client = bs.Client("127.0.0.1:11300");
  client.use(mongoCollection).onSuccess(data => {
    client
      .put(JSON.stringify(elem))
      .onSuccess(data => {
        console.log("insertado", elem.name);
        client.disconnect();
      })
      .onError(e => {
        console.log("#########error###########3", e);

        client.disconnect();
      });
  });
};
const enqueuePrev = async elem => {
  await enqueue(elem);
};
const getEpisodes = async film => {
  const episodes = {};
  await axios
    .get(
      `https://apis.justwatch.com/content/titles/show/${
        film.id_justwatch
      }/locale/es_ES`
    )
    .then(async response => {
      const seasons = response.data.seasons;
      for (let i = 0; i < seasons.length; i++) {
        await axios
          .get(
            `https://apis.justwatch.com/content/urls?path=${
              seasons[i].full_path
            }`
          )
          .then(async r => {
            await axios
              .get(
                `https://apis.justwatch.com/content/titles/show_season/${
                  r.data.object_id
                }/locale/es_ES`
              )
              .then(res => {
                const ep = [];
                for (let ii = 0; ii < res.data.episodes.length; ii++) {
                  const {
                    id,
                    title,
                    poster,
                    season_number,
                    episode_number,
                    show_title,
                    short_description
                  } = res.data.episodes[ii];
                  ep.push({
                    id,
                    title,
                    poster,
                    season_number,
                    episode_number,
                    show_title,
                    short_description
                  });
                }
                episodes[seasons[i].season_number] = ep;
              });
          });
      }
    });
  return episodes;
};
const bannedIps = [];
const getFilmAffinity = async film => {
  const proxy = Proxy.getProxy();

  return request.get(
    {
      uri: baseFilmaffinityUrl + film.name.replace(" ", "-"),
      headers: {
        "User-Agent":
          "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16"
      },
      proxy: `http://androidlistaShared:x8h91xP5@${proxy}`
    },
    async (err, response, body) => {
      const status =
        response === undefined
          ? "429 Too Many Requests"
          : response.headers.status;
      let bodyFinal = "";
      if (status === "429 Too Many Requests") {
        console.log("banned ip", proxy);
        bannedIps.push(proxy);
        bodyFinal = "";
      } else {
        bodyFinal = body;
      }
      const $ = cheerio.load(bodyFinal);
      let filmaffinity = [];
      let img = "";
      let name = "";
      let id = "";
      let note = "";
      let url = "";
      let noteYomvi = "";
      let year = "";
      let director = "";
      if ($(".se-it").length > 0) {
        $(".se-it").each((i, elm) => {
          id = $(elm)
            .find(".movie-card-1")
            .attr("data-movie-id");
          img = $(elm)
            .find("img")
            .attr("src")
            .replace("-msmall.jpg", "-mmed.jpg");
          name = $(elm)
            .find(".mc-title a")
            .text()
            .replace(" (Serie de TV)", "")
            .replace(" (Miniserie de TV)", "");
          year = $(elm)
            .find(".ye-w")
            .text();
          url = $(elm)
            .find(".mc-title a")
            .attr("href");
          note = $(elm)
            .find(".mr-rating .avgrat-box")
            .text();
          director = $(elm)
            .find(".mc-director .credits span a")
            .text();
          if (
            slugify(film.name.toLowerCase()) === slugify(name.toLowerCase()) &&
            parseInt(year) === parseInt(film.year)
          ) {
            noteYomvi = note;
            filmaffinity.push({
              id,
              name,
              url,
              year,
              img,
              note: note !== undefined ? note.replace(",", ".") : -1
            });
          } else if (
            parseInt(year) === parseInt(film.year) &&
            slugify(film.director.toLowerCase()) ===
              slugify(director.toLowerCase())
          ) {
            filmaffinity.push({
              id,
              name,
              url,
              year,
              img,
              note: note !== undefined ? note.replace(",", ".") : -1
            });
          }
        });

        if (filmaffinity.length > 1) {
          const filmaffinityAux = [];
          filmaffinity.map(f => {
            if (parseInt(f.year) === parseInt(film.year)) {
              noteYomvi = f.note;
              filmaffinityAux.push(f);
            }
          });
          filmaffinity = filmaffinityAux;
        }
      } else {
        $("#mt-content-cell").each((i, elm) => {
          id = $(elm)
            .find(".rate-movie-box")
            .attr("data-movie-id");
          img = $(elm)
            .find("#movie-main-image-container img")
            .attr("src");
          name = film.name;
          year = $(elm)
            .find("dd[itemprop = 'datePublished']")
            .text();
          url = $(elm)
            .find(".margin-ntabs .ntabs .active a")
            .attr("href");
          note = $(elm)
            .find("#rat-avg-container #movie-rat-avg")
            .attr("content");
          noteYomvi = note;
          if (id !== undefined) {
            filmaffinity.push({
              id,
              name,
              url,
              year,
              img,
              note: note !== undefined ? note.replace(",", ".") : -1
            });
          }
        });
      }
      let elem = "";
      if (filmaffinity.length > 0) {
        elem = Object.assign(film, { filmaffinity });
      } else {
        elem = Object.assign(film);
      }
      if (noteYomvi !== undefined) {
        const n = parseFloat(noteYomvi.replace(",", "."));
        if (!Number.isNaN(n)) {
          elem.note_filmaffinity = n;
        }
      }
      elem.description = film.description === undefined ? "" : film.description;
      elem.is_active = 1;
      elem._id = elem.id;

      if (elem.showType === "show") {
        const episodes = await getEpisodes(elem);
        elem.episodes = episodes;
      }
      enqueue(elem);
    }
  );
  console.log("ips baneados", bannedIps);
  return true;
};

const getDirector = credits => {
  let director = "";
  if (credits !== undefined) {
    credits.map(c => {
      if (c.role === "DIRECTOR") {
        director = c.name;
      }
    });
  }

  return director;
};
const getActors = credits => {
  const actors = [];
  if (credits !== undefined) {
    credits.map(c => {
      if (c.role === "ACTOR") {
        actors.push(c.name);
      }
    });
  }

  return actors;
};
const getFilmInfo = async (responsePlatforms, showType, genero, type, f) => {
  const platforms = {};
  let trailer = "";
  const scores = [];
  if (responsePlatforms.data.offers !== undefined) {
    responsePlatforms.data.offers.map(p => {
      if (p.urls.standard_web !== undefined) {
        platforms[p.provider_id] = p.urls.standard_web;
      }
    });
  }
  if (responsePlatforms.data.clips !== undefined) {
    responsePlatforms.data.clips.map(c => {
      if (c.type === "trailer") {
        trailer = c.external_id;
      }
    });
  }
  if (responsePlatforms.data !== undefined) {
    responsePlatforms.data.scoring.map(s => {
      scores[s.provider_type] = s.value;
    });
  }
  const noteImdb =
    scores["imdb:score"] !== undefined ? scores["imdb:score"] : undefined;
  const director = getDirector(responsePlatforms.data.credits);
  const actors = getActors(responsePlatforms.data.credits);
  const generoNombre = `genero_${genero}`;
  const fi = {
    id: `${showType}_${f.id}`,
    id_justwatch: f.id,
    name: f.title,
    description: f.short_description,
    year: f.original_release_year,
    image: `https://images.justwatch.com${f.poster.replace(
      "{profile}",
      "s166"
    )}`,
    ...(responsePlatforms.data.backdrops &&
    responsePlatforms.data.backdrops.length > 0
      ? {
          banner: `https://images.justwatch.com${responsePlatforms.data.backdrops[0].backdrop_url.replace(
            "{profile}",
            "s1440"
          )}`
        }
      : {}),
    [`platform_${type}`]: type,
    showType,
    director,
    actors,
    platforms,
    [generoNombre]: genero
  };
  if (trailer !== "") {
    fi.trailer = trailer;
  }
  if (noteImdb !== undefined) {
    fi.note_imdb = noteImdb;
  }
  films.push(fi);
  //console.log('antes');
  await getFilmAffinity(fi);
  //console.log('despues');
  return films;
};
const getPlatformUrls = (url, genero, type, showType) =>
  axios.get(url).then(response =>
    response.data.items.map(f =>
      axios
        .get(
          `https://apis.justwatch.com/content/titles/${showType}/${
            f.id
          }/locale/es_ES`
        )
        .then(responsePlatforms => {
          getFilmInfo(responsePlatforms, showType, genero, type, f);
        })
    )
  );

const getYomviUrls = async (url, genero) => {
  const response = await axios.get(url);
  const $ = cheerio.load(response.data);

  $(".slider-box-item").each((i, elm) => {
    films.push({
      id: $(elm)
        .find(".miniFicha")
        .attr("data-elemento"),
      name: $(elm)
        .find(".title-box p")
        .text(),
      image: $(elm)
        .find(".enlaceFicha img")
        .attr("src"),
      url: $(elm)
        .find(".enlaceFicha")
        .attr("href"),
      genero
    });
  });
  return films;
};
const importYomvi = async (url, genero, type) => {
  const yomvi = await getYomviUrls(url, genero, type);
  //console.log(yomvi);
  for (let i = 0; i < yomvi.length; i++) {
    getJustwatch(yomvi[i], type);
  }
};
const getJustwatch = async ({ name, url }, showType) => {
  const urlApi = `https://apis.justwatch.com/content/titles/es_ES/popular?body=%7B%22content_types%22:%5B%22${showType}%22%5D,%22page%22:1,%22page_size%22:7,%22query%22:%22${name
    .split(" ")
    .join("+")}%22%7D`;
  const films = await axios.get(urlApi);
  const selectedFilters = films.data.items.filter(
    film =>
      slugify(
        film.title
          .replace("(", "")
          .replace(")", "")
          .replace(".", "")
          .toLowerCase()
      ) ===
      slugify(
        name
          .replace("(", "")
          .replace(")", "")
          .replace(".", "")
          .toLowerCase()
      )
  );
  if (selectedFilters.length > 0) {
    axios
      .get(
        `https://apis.justwatch.com/content/titles/${showType}/${
          selectedFilters[0].id
        }/locale/es_ES`
      )
      .then(responsePlatforms => {
        getFilmInfoYomvi(responsePlatforms, showType, url);
      });
  }
};

const getFilmInfoYomvi = async (responsePlatforms, showType, urlYomvi) => {
  let trailer = "";
  const scores = [];
  const platformsFormat = {
    platform_netflix: "",
    platform_hbo: "",
    platform_yomvi: urlYomvi ? "yomvi" : "",
    platform_amazon: "",
    platform_filmin: ""
  };
  const platformUrl = urlYomvi
    ? { 149: `http://ver.movistarplus.es${urlYomvi}` }
    : {};
  const platformsAux =
    responsePlatforms.data.offers &&
    responsePlatforms.data.offers.filter(f => f.provider_id !== 149).reduce(
      (acc, curr, key) => {
        const platformName = getPlatformName(curr.provider_id.toString());
        if (platformName) {
          acc.platforms[curr.provider_id] = curr.urls.standard_web;
          acc[`platform_${platformName}`] = platformName;
        } else {
          acc[`platform_${platformName}`] = "";
        }
        return acc;
      },
      {
        ...platformsFormat,
        platforms: { ...platformUrl }
      }
    );
  console.log("joooooooooooooderrrrr", platformUrl);
  const platforms = platformsAux || { ...platformsFormat };
  if (responsePlatforms.data.clips !== undefined) {
    responsePlatforms.data.clips.map(c => {
      if (c.type === "trailer") {
        trailer = c.external_id;
      }
    });
  }
  if (responsePlatforms.data !== undefined) {
    responsePlatforms.data.scoring.map(s => {
      scores[s.provider_type] = s.value;
    });
  }

  const noteImdb =
    scores["imdb:score"] !== undefined ? scores["imdb:score"] : undefined;
  const director = getDirector(responsePlatforms.data.credits);
  const actors = getActors(responsePlatforms.data.credits);
  if (!responsePlatforms.data.genre_ids) return false;
  const generos = responsePlatforms.data.genre_ids.reduce((acc, curr) => {
    const generoNombre = genreMapping(curr);
    acc[`genero_${generoNombre}`] = generoNombre;
    return acc;
  }, {});
  const fi = {
    id: `${showType}_${responsePlatforms.data.id}`,
    id_justwatch: responsePlatforms.data.id,
    name: responsePlatforms.data.title,
    description: responsePlatforms.data.short_description,
    year: responsePlatforms.data.original_release_year,
    image: `https://images.justwatch.com${responsePlatforms.data.poster.replace(
      "{profile}",
      "s166"
    )}`,
    ...(responsePlatforms.data.backdrops &&
    responsePlatforms.data.backdrops.length > 0
      ? {
          banner: `https://images.justwatch.com${responsePlatforms.data.backdrops[0].backdrop_url.replace(
            "{profile}",
            "s1440"
          )}`
        }
      : {}),
    showType,
    director,
    actors,
    ...platforms,
    ...generos
  };
  if (trailer !== "") {
    fi.trailer = trailer;
  }
  if (noteImdb !== undefined) {
    fi.note_imdb = noteImdb;
  }
  // console.log('antes');
  await getFilmAffinity(fi);
  // console.log('despues');
  console.log(fi);
  return fi;
};
console.log("Connected correctly to server");
if (url !== undefined) {
  importYomvi(url, genero, "movie");
  importYomvi(url.replace("portada_cine", "portada_series"), genero, "show");
}
if (urlNetflix !== undefined) {
  const promisesNetflix = [];
  promisesNetflix.push(getPlatformUrls(urlNetflix, genero, "netflix", "movie"));
  promisesNetflix.push(
    getPlatformUrls(
      urlNetflix.replace(":%5B%22movie%22%5D,", ":%5B%22show%22%5D,"),
      genero,
      "netflix",
      "show"
    )
  );
}

if (urlHBO !== undefined) {
  const promisesHBO = [];
  promisesHBO.push(getPlatformUrls(urlHBO, genero, "hbo", "movie"));
  promisesHBO.push(
    getPlatformUrls(
      urlHBO.replace(":%5B%22movie%22%5D,", ":%5B%22show%22%5D,"),
      genero,
      "hbo",
      "show"
    )
  );
}

if (urlAMZ !== undefined) {
  const promisesAMZ = [];
  promisesAMZ.push(getPlatformUrls(urlAMZ, genero, "amazon", "movie"));
  promisesAMZ.push(
    getPlatformUrls(
      urlAMZ.replace(":%5B%22movie%22%5D,", ":%5B%22show%22%5D,"),
      genero,
      "amazon",
      "show"
    )
  );
}

if (urlFilmin !== undefined) {
  const promisesFilmin = [];
  promisesFilmin.push(getPlatformUrls(urlFilmin, genero, "filmin", "movie"));
  promisesFilmin.push(
    getPlatformUrls(
      urlFilmin.replace(":%5B%22movie%22%5D,", ":%5B%22show%22%5D,"),
      genero,
      "filmin",
      "show"
    )
  );
}

if (urlRakuten !== undefined) {
  const promisesRakuten = [];
  promisesRakuten.push(getPlatformUrls(urlRakuten, genero, "rakuten", "movie"));
  promisesRakuten.push(
    getPlatformUrls(
      urlRakuten.replace(":%5B%22movie%22%5D,", ":%5B%22show%22%5D,"),
      genero,
      "filmin",
      "show"
    )
  );
}
