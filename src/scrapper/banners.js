const axios = require("axios");
const cheerio = require("cheerio");
const MongoClient = require("mongodb").MongoClient,
  assert = require("assert");
let bs = require("nodestalker"),
  client = bs.Client("127.0.0.1:11300");
const {
  fireStoreCollection,
  generosValues,
  platformsValues,
  platforms,
  generosArray,
  mongoCollection,
  mongoUrl
} = require("../constants.js");

const importBanner = film => {
  const url = `https://apis.justwatch.com/content/titles/${film.showType}/${
    film.id_justwatch
  }/locale/es_ES`;

  return film.showType && axios.get(url).then(result => {
    const banner =
      result.data.backdrops &&
      result.data.backdrops.length > 0 &&
      `https://images.justwatch.com${result.data.backdrops[0].backdrop_url.replace(
        "{profile}",
        "s1440"
      )}`;
      console.log("url", url);
    console.log("banner", banner);

    banner &&
      client.use(mongoCollection).onSuccess(data => {
        console.log(data);
        film.banner = banner;
        client.put(JSON.stringify(film)).onSuccess(data => {
          console.log(data);
          // client.disconnect();
        });
      });
  });
};

MongoClient.connect(mongoUrl, (e, db) => {
  const collection = db.collection(mongoCollection);
  collection.find({ banner: { $exists: false } }).toArray((errr, films) => {
    films.map(film => {
      importBanner(film);
    });
    db.close();
  });
});
