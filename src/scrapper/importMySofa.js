const bs = require('nodestalker');

const axios = require('axios');
const request = require('request');
const cheerio = require('cheerio');
const slugify = require('slugify');
const filmsMysofa = require('../mysofa.js');

const { Proxy } = require('./Proxy.js');
const {
  baseFilmaffinityUrl, baseYomviUrl, baseNetflixUrl, baseHBOUrl, baseFilminUrl, baseAMZUrl, mongoUrl, mongoCollection,
} = require('../constants.js');

const enqueue = (elem) => {
  const client = bs.Client('127.0.0.1:11300');
  client.use(mongoCollection).onSuccess((data) => {
  //  console.log(data);

    client.put(JSON.stringify(elem)).onSuccess((data) => {
      console.log('insertado', elem.name);
      	client.disconnect();
    }).onError((e) => {
      console.log('#########error###########3', elem.name);

      client.disconnect();
    });
  });
};
const getEpisodes = async (film) => {
  const episodes = {};
  await axios.get(`https://apis.justwatch.com/content/titles/show/${film.id_justwatch}/locale/es_ES`).then(async (response) => {
    const seasons = response.data.seasons;
    for (let i = 0; i < seasons.length; i++) {
      await axios.get(`https://apis.justwatch.com/content/urls?path=${seasons[i].full_path}`).then(async (r) => {
        await axios.get(`https://apis.justwatch.com/content/titles/show_season/${r.data.object_id}/locale/es_ES`).then((res) => {
          const ep = [];
          if (!res.data.episodes) return {};
          for (let ii = 0; ii < res.data.episodes.length; ii++) {
            const {
              id, title, poster, season_number, episode_number, show_title, short_description,
            } = res.data.episodes[ii];
            ep.push({
              id, title, poster, season_number, episode_number, show_title, short_description,
            });
          }
          episodes[seasons[i].season_number] = ep;
        });
      });
    }
  });
  return episodes;
};
const getFilmAffinity = async (film) => {
  const proxy = Proxy.getProxy();

  return request.get({
    uri: baseFilmaffinityUrl + film.name.replace(' ', '-'),
    headers: {
      'User-Agent': 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16',
    },
    proxy: `http://androidlistaShared:x8h91xP5@${proxy}`,
  }, async (err, response, body) => {
    const status = (response === undefined) ? '429 Too Many Requests' : response.headers.status;
    let bodyFinal = '';
    if (status === '429 Too Many Requests') {
      console.log('banned ip', proxy);
      bannedIps.push(proxy);
      bodyFinal = '';
    } else {
      bodyFinal = body;
    }
    const $ = cheerio.load(bodyFinal);
    let filmaffinity = [];
    let img = '';
    let name = '';
    let id = '';
    let note = '';
    let url = '';
    let noteYomvi = '';
    let year = '';
    let director = '';
    if ($('.se-it').length > 0) {
      $('.se-it').each((i, elm) => {
        id = $(elm).find('.movie-card-1').attr('data-movie-id');
        img = $(elm).find('img').attr('src').replace('-msmall.jpg', '-mmed.jpg');
        name = $(elm).find('.mc-title a').text().replace(' (Serie de TV)', '')
          .replace(' (Miniserie de TV)', '');
        year = $(elm).find('.ye-w').text();
        url = $(elm).find('.mc-title a').attr('href');
        note = $(elm).find('.mr-rating .avgrat-box').text();
        director = $(elm).find('.mc-director .credits span a').text();
        if (slugify(film.name.toLowerCase()) === slugify(name.toLowerCase()) && parseInt(year) === parseInt(film.year)) {
          noteYomvi = note;
          filmaffinity.push({
            id,
            name,
            url,
            year,
            img,
            note: (note !== undefined) ? note.replace(',', '.') : -1,
          });
        } else if (parseInt(year) === parseInt(film.year) && slugify(film.director.toLowerCase()) === slugify(director.toLowerCase())) {
          // console.log('pepepeepe', film.director);
          filmaffinity.push({
            id,
            name,
            url,
            year,
            img,
            note: (note !== undefined) ? note.replace(',', '.') : -1,
          });
        }
      });


      if (filmaffinity.length > 1) {
        const filmaffinityAux = [];
        filmaffinity.map((f) => {
          if (parseInt(f.year) === parseInt(film.year)) {
            noteYomvi = f.note;
            //  console.log('entra');
            filmaffinityAux.push(f);
          }
        });
        filmaffinity = filmaffinityAux;
      }
    } else {
      $('#mt-content-cell').each((i, elm) => {
        id = $(elm).find('.rate-movie-box').attr('data-movie-id');
        img = $(elm).find('#movie-main-image-container img').attr('src');
        name = film.name;
        year = $(elm).find("dd[itemprop = 'datePublished']").text();
        url = $(elm).find('.margin-ntabs .ntabs .active a').attr('href');
        note = $(elm).find('#rat-avg-container #movie-rat-avg').attr('content');
        noteYomvi = note;
        if (id !== undefined) {
          filmaffinity.push({
            id,
            name,
            url,
            year,
            img,
            note: (note !== undefined) ? note.replace(',', '.') : -1,
          });
        }
      });
    }
    let elem = '';
    if (filmaffinity.length > 0) {
      elem = Object.assign(film, { filmaffinity });
    } else {
      elem = Object.assign(film);
    }
    if (noteYomvi !== undefined) {
      elem.note_filmaffinity = parseFloat(noteYomvi.replace(',', '.'));
    }
    elem.description = (film.description === undefined) ? '' : film.description;
    elem.is_active = 1;
    elem._id = elem.id;
    //  console.log('polla', elem);
    // console.log('insercion', `${elem.id} ${elem.showType}`);

    // console.log('precio al update');

    if (elem.showType === 'show') {
      const episodes = await getEpisodes(elem);
      elem.episodes = episodes;
    }
    enqueue(elem);
  });
  console.log('ips baneados', bannedIps);
  return true;
};

const getDirector = (credits) => {
  let director = '';
  if (credits !== undefined) {
    credits.map((c) => {
      if (c.role === 'DIRECTOR') {
        director = c.name;
      }
    });
  }

  return director;
};
const getActors = (credits) => {
  const actors = [];
  if (credits !== undefined) {
    credits.map((c) => {
      if (c.role === 'ACTOR') {
        actors.push(c.name);
      }
    });
  }

  return actors;
};

const getPlatformName = (platform) => {
  switch (platform) {
    case '8':
      return 'netflix';
    case '118':
      return 'hbo';
    case '149':
      return 'yomvi';
    case '119':
      return 'amazon';
    case '63':
      return 'filmin';
    case '64':
      return 'filmin';
    default:
      return undefined;
  }
};
const genreMapping = (genreId) => {
  const genres = {
    1: 'accion',
    3: 'comedia',
    4: 'policiaca',
    5: 'documental',
    6: 'drama',
    7: 'fantasia',
    8: 'historia',
    9: 'terror',
    10: 'infantil',
    11: 'musical',
    12: 'suspense',
    13: 'romantica',
    14: 'scfi',
    15: 'deportes',
    16: 'belico',
    17: 'western',
  };

  return genres[genreId];
};
const getFilmInfo = async (responsePlatforms, showType) => {
  let trailer = '';
  const scores = [];
  const platformsFormat = {
    platform_netflix: '',
    platform_hbo: '',
    platform_yomvi: '',
    platform_amazon: '',
    platform_filmin: '',
  };
  const platformsAux = responsePlatforms.data.offers && responsePlatforms.data.offers.reduce((acc, curr, key) => {
    const platformName = getPlatformName(curr.provider_id.toString());
    if (platformName) {
      acc.platforms[curr.provider_id] = curr.urls.standard_web;
      acc[`platform_${platformName}`] = platformName;
    } else {
      acc[`platform_${platformName}`] = '';
    }
    return acc;
  }, { ...platformsFormat, platforms: {} });
  const platforms = platformsAux || { ...platformsFormat };
  if (responsePlatforms.data.clips !== undefined) {
    responsePlatforms.data.clips.map((c) => {
      if (c.type === 'trailer') {
        trailer = c.external_id;
      }
    });
  }
  if (responsePlatforms.data !== undefined) {
    responsePlatforms.data.scoring.map((s) => {
      scores[s.provider_type] = s.value;
    });
  }
  const noteImdb = (scores['imdb:score'] !== undefined) ? scores['imdb:score'] : undefined;
  const director = getDirector(responsePlatforms.data.credits);
  const actors = getActors(responsePlatforms.data.credits);
  if (!responsePlatforms.data.genre_ids) return false;
  const generos = responsePlatforms.data.genre_ids.reduce((acc, curr) => {
    const generoNombre = genreMapping(curr);
    acc[`genero_${generoNombre}`] = generoNombre;
    return acc;
  }, {});
  const fi = {
    id: `${showType}_${responsePlatforms.data.id}`,
    id_justwatch: responsePlatforms.data.id,
    name: responsePlatforms.data.title,
    description: responsePlatforms.data.short_description,
    year: responsePlatforms.data.original_release_year,
    image: `https://images.justwatch.com${responsePlatforms.data.poster.replace('{profile}', 's166')}`,
    showType,
    director,
    actors,
    ...platforms,
    ...generos,
  };
  if (trailer !== '') {
    fi.trailer = trailer;
  }
  if (noteImdb !== undefined) {
    fi.note_imdb = noteImdb;
  }
  // console.log('antes');
  await getFilmAffinity(fi);
  // console.log('despues');
  console.log(fi);
  return fi;
};

const start = async () => {
  for (let i = 0; i < filmsMysofa.films.length; i++) {
    try {
      const f = filmsMysofa.films[i];
      const urlFinal = `https://apis.justwatch.com/content/urls?include_children=true&path=/es/pelicula/${slugify(f.toLowerCase())}`;
      console.log(urlFinal);
      const response = await axios.get(urlFinal);
      const responsePlatforms = await axios.get(`https://apis.justwatch.com/content/titles/${response.data.object_type}/${response.data.object_id}/locale/es_ES`);
      getFilmInfo(responsePlatforms, response.data.object_type);
    } catch (e) { }
  }
};
start();
/*
scraper.search(options, (err, url) => {
  // This is called for each result
  if (err) throw err;
  console.log(url);

  const urlFinal = `https://apis.justwatch.com/content/urls?include_children=true&path=${url.replace('https://www.justwatch.com', '')}`;

  axios.get(urlFinal).then(response => axios.get(`https://apis.justwatch.com/content/titles/${response.data.object_type}/${response.data.object_id}/locale/es_ES`).then((responsePlatforms) => {
    getFilmInfo(responsePlatforms, response.data.object_type);
  }));
}); */
