const axios = require('axios');
const cheerio = require('cheerio');
const request = require('request');
const slugify = require('slugify');
const { db } = require('./firebase.js');
const {
  baseFilmaffinityUrl, baseYomviUrl, baseNetflixUrl, baseHBOUrl, baseFilminUrl, baseAMZUrl, fireStoreCollection,
} = require('../constants.js');
const { Proxy } = require('./Proxy.js');

let url = '';
let urlNetflix = '';
let urlHBO = '';
let urlAMZ = '';
let urlFilmin = '';
let genero = '';
if (process.argv.length === 2) {
  console.log('Envia un genero: accion, animacion, aventura, scfi, independiente, infantil, drama, suspense, terror ...');
  process.exit();
} else {
  genero = process.argv[2];
  url = baseYomviUrl[genero];
  urlNetflix = baseNetflixUrl[genero];
  urlHBO = baseHBOUrl[genero];
  urlAMZ = baseAMZUrl[genero];
  urlFilmin = baseFilminUrl[genero];
}

const films = [];

const getYomviUrls = (url, genero) => axios.get(url).then((response) => {
  const $ = cheerio.load(response.data);

  $('.slider-box-item').each((i, elm) => {
    const idAux = $(elm).find('.miniFicha').attr('data-elemento');
    const id = `yomvi_${idAux}`;
    films.push({
      id,
      name: $(elm).find('.title-box p').text(),
      image: $(elm).find('.enlaceFicha img').attr('src'),
      genero,
      type: 'yomvi',
    });
  });
  return (films);
});
const getImdb = (film) => {
//  console.log(`http://www.imdb.com/find?q=${encodeURIComponent(film.name)}&s=tt&exact=true&ref_=fn_tt_ex`);
  const proxy = Proxy.getProxy();
  return request.get({
    uri: `http://www.imdb.com/find?q=${encodeURIComponent(film.name)}&s=tt&ref_=fn_al_tt_mr`,
    headers: {
      'User-Agent': 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16',
    },
    proxy: `http://androidlistaShared:x8h91xP5@${proxy}`,
  }, (err, response, body) => {
    const bodyFinal = (body !== undefined) ? body : '';
    const $ = cheerio.load(bodyFinal);
    const noteYomvi = '';
    if ($('.findList').length > 0) {
      const filmArray = [];
      $('.findResult').each((i, elm) => {
        const urlCall = $(elm).find('.result_text a').attr('href');
        const name = $(elm).find('.result_text a').text();
        const yearAux = $(elm).find('.result_text').text().replace(name, '')
          .match(/\d/g);
        const year = (yearAux !== null) ? parseInt(yearAux
          .join('')
          .trim())
          :
          '';
        // console.log("@@@@@@@@@@@@@@@########", year+" /"+ name);
        if ((filmArray.length === 0 && slugify(film.name.toLowerCase()) === slugify(name.toLowerCase()) && year === parseInt(film.year)) || (filmArray.length === 0 && slugify(film.name.toLowerCase()) !== slugify(name.toLowerCase()) && year === parseInt(film.year))) {
          request.get({
            uri: `https://www.imdb.com${urlCall}`,
            headers: {
              'User-Agent': 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16',
            },
            proxy: `http://androidlistaShared:x8h91xP5@${proxy}`,
          }, (err2, response2, body2) => {
            const body2Final = (body2 !== undefined) ? body2 : '';
            const $2 = cheerio.load(body2Final);
            if ($2('.ratingValue strong span').length > 0) {
              const note = $2('.ratingValue strong span').text();
              const id = $2('meta[property="pageId"]').attr('content');
              const urlFinal = $2('meta[property="og:url"]').attr('content');
              const img = $2('img[itemprop="image"]').attr('src');
              const nameFinal = $2('h1').text();
              const yearFinal = $2('#titleYear a').text();
              const director = $2('span[itemprop="director"]').text().trim();


              const inserta = ((slugify(film.name.toLowerCase()) === slugify(name.toLowerCase()) && year === parseInt(film.year)) || director.split(' ').pop() === film.director.split(' ').pop());
              if (inserta) {
                const imdb = {
                  id,
                  name: nameFinal,
                  url: urlFinal,
                  year: yearFinal,
                  img,
                  director,
                  note: (note !== undefined) ? note.replace(',', '.') : -1,
                };
                const imdbArray = [imdb];

                const elem = Object.assign(film, { imdb: imdbArray });
                elem.note_imdb = (note !== undefined) ? parseFloat(note.replace(',', '.')) : -1;
                elem.description = (film.description === undefined) ? '' : film.description;
                elem.is_active = 1;
                // console.log(film.name, elem);
                // console.log('insercion', `${elem.id} ${elem.showType}`);
                const docRef = db.collection(fireStoreCollection).doc(elem.id);
                docRef.set(elem, { merge: true });
              } else {
                console.log('cuidao que no entra', `${slugify(film.name.toLowerCase())} ${slugify(name.toLowerCase())} ${year} ${film.year}`);
              }
            }
          });
          filmArray.push(slugify(film.name.toLowerCase()));
        } else {
        //  console.log(slugify(film.name.toLowerCase()), `${film.name} ${slugify(name.toLowerCase())} ${year}`);
        }
      });
    }
  });
};
const bannedIps = [];
const getFilmAffinity = (film) => {
  const proxy = Proxy.getProxy();

  return request.get({
    uri: baseFilmaffinityUrl + film.name.replace(' ', '-'),
    headers: {
      'User-Agent': 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16',
    },
    proxy: `http://androidlistaShared:x8h91xP5@${proxy}`,
  }, (err, response, body) => {
    const status = (response === undefined) ? '429 Too Many Requests' : response.headers.status;
    let bodyFinal = '';
    if (status === '429 Too Many Requests') {
      console.log('banned ip', proxy);
      bannedIps.push(proxy);
      bodyFinal = '';
    } else {
      bodyFinal = body;
    }
    const $ = cheerio.load(bodyFinal);
    let filmaffinity = [];
    let img = '';
    let name = '';
    let id = '';
    let note = '';
    let url = '';
    let noteYomvi = '';
    let year = '';
    let director = '';
    if ($('.se-it').length > 0) {
      $('.se-it').each((i, elm) => {
        id = $(elm).find('.movie-card-1').attr('data-movie-id');
        img = $(elm).find('img').attr('src').replace('-msmall.jpg', '-mmed.jpg');
        name = $(elm).find('.mc-title a').text().replace(' (Serie de TV)', '')
          .replace(' (Miniserie de TV)', '');
        year = $(elm).find('.ye-w').text();
        url = $(elm).find('.mc-title a').attr('href');
        note = $(elm).find('.mr-rating .avgrat-box').text();
        director = $(elm).find('.mc-director .credits span a').text();
        if (slugify(film.name.toLowerCase()) === slugify(name.toLowerCase()) && parseInt(year) === parseInt(film.year)) {
          noteYomvi = note;
          filmaffinity.push({
            id,
            name,
            url,
            year,
            img,
            note: (note !== undefined) ? note.replace(',', '.') : -1,
          });
        } else if (parseInt(year) === parseInt(film.year) && slugify(film.director.toLowerCase()) === slugify(director.toLowerCase())) {
          // console.log('pepepeepe', film.director);
          filmaffinity.push({
            id,
            name,
            url,
            year,
            img,
            note: (note !== undefined) ? note.replace(',', '.') : -1,
          });
        }
      });


      if (filmaffinity.length > 1) {
        const filmaffinityAux = [];
        filmaffinity.map((f) => {
          if (parseInt(f.year) === parseInt(film.year)) {
            noteYomvi = f.note;
            //  console.log('entra');
            filmaffinityAux.push(f);
          }
        });
        filmaffinity = filmaffinityAux;
      }
    } else {
      $('#mt-content-cell').each((i, elm) => {
        id = $(elm).find('.rate-movie-box').attr('data-movie-id');
        img = $(elm).find('#movie-main-image-container img').attr('src');
        name = film.name;
        year = $(elm).find("dd[itemprop = 'datePublished']").text();
        url = $(elm).find('.margin-ntabs .ntabs .active a').attr('href');
        note = $(elm).find('#rat-avg-container #movie-rat-avg').attr('content');
        noteYomvi = note;
        if (id !== undefined) {
          filmaffinity.push({
            id,
            name,
            url,
            year,
            img,
            note: (note !== undefined) ? note.replace(',', '.') : -1,
          });
        }
      });
    }
    let elem = '';
    if (filmaffinity.length > 0) {
      elem = Object.assign(film, { filmaffinity });
    } else {
      elem = Object.assign(film);
    }
    if (noteYomvi !== undefined) {
      elem.note_filmaffinity = parseFloat(noteYomvi.replace(',', '.'));
    }
    elem.description = (film.description === undefined) ? '' : film.description;
    elem.is_active = 1;
    //  console.log('polla', elem);
    console.log('insercion', `${elem.id} ${elem.showType}`);
    const docRef = db.collection(fireStoreCollection).doc(elem.id);
    docRef.set(elem, { merge: true });
    console.log('ips baneados', bannedIps);
    return true;
  });
};
const getDirector = (credits) => {
  let director = '';
  credits.map((c) => {
    if (c.role === 'DIRECTOR') {
      director = c.name;
    }
  });
  return director;
};
const getActors = (credits) => {
  const actors = [];
  credits.map((c) => {
    if (c.role === 'ACTOR') {
      actors.push(c.name);
    }
  });
  return actors;
};
const getPlatformUrls = (url, genero, type, showType) => axios.get(url).then(response => response.data.items.map(f => axios.get(`https://apis.justwatch.com/content/titles/${showType}/${f.id}/locale/es_ES`).then((responsePlatforms) => {
  const platforms = {};
  let trailer = '';
  const scores = [];
  if (responsePlatforms.data.offers !== undefined) {
    responsePlatforms.data.offers.map((p) => {
      if (p.urls.standard_web !== undefined) {
        platforms[p.provider_id] = p.urls.standard_web;
      }
    });
  }

  if (responsePlatforms.data.clips !== undefined) {
    responsePlatforms.data.clips.map((c) => {
      if (c.type === 'trailer') {
        trailer = c.external_id;
      }
    });
  }
  if (responsePlatforms.data !== undefined) {
    responsePlatforms.data.scoring.map((s) => {
      scores[s.provider_type] = s.value;
    });
  }

  const noteImdb = (scores['imdb:score'] !== undefined) ? scores['imdb:score'] : undefined;
  const director = getDirector(responsePlatforms.data.credits);
  const actors = getActors(responsePlatforms.data.credits);
  const generoNombre = `genero_${genero}`;
  const fi = {
    id: `${showType}_${f.id}`,
    name: f.title,
    description: f.short_description,
    year: f.original_release_year,
    image: `https://images.justwatch.com${f.poster.replace('{profile}', 's166')}`,
    [`platform_${type}`]: type,
    showType,
    director,
    actors,
    platforms,
    [generoNombre]: genero,
  };
  if (trailer !== '') {
    fi.trailer = trailer;
  }
  if (noteImdb !== undefined) {
    fi.note_imdb = noteImdb;
  }
  films.push(f);
  getFilmAffinity(fi);
  // getImdb(fi);
  //  console.log(films);
  return (films);
})));


const batch = db.batch();
const generoQuery = `genero_${genero}`;
db.collection('films').where('is_active', '==', 1).where(generoQuery, '==', genero).get()
  .then((snapshot) => {
    console.log('EMPEZAMOS');
    /*  snapshot.forEach((doc) => {
      batch.set(doc._ref, { is_active: 0 }, { merge: true });
    });
    batch.commit(); */
    console.log('borrado finalizado');
    if (url !== undefined) {
      const promisesYomvi = [];
      promisesYomvi.push(getPlatformUrls(url, genero, 'yomvi', 'movie'));
      promisesYomvi.push(getPlatformUrls(url.replace(':%5B%22movie%22%5D,', ':%5B%22show%22%5D,'), genero, 'yomvi', 'show'));
    }
    if (urlNetflix !== undefined) {
      const promisesNetflix = [];
      promisesNetflix.push(getPlatformUrls(urlNetflix, genero, 'netflix', 'movie'));
      promisesNetflix.push(getPlatformUrls(urlNetflix.replace(':%5B%22movie%22%5D,', ':%5B%22show%22%5D,'), genero, 'netflix', 'show'));
    }

    if (urlHBO !== undefined) {
      const promisesHBO = [];
      promisesHBO.push(getPlatformUrls(urlHBO, genero, 'hbo', 'movie'));
      promisesHBO.push(getPlatformUrls(urlHBO.replace(':%5B%22movie%22%5D,', ':%5B%22show%22%5D,'), genero, 'hbo', 'show'));
    }

    if (urlAMZ !== undefined) {
      const promisesAMZ = [];
      promisesAMZ.push(getPlatformUrls(urlAMZ, genero, 'amazon', 'movie'));
      promisesAMZ.push(getPlatformUrls(urlAMZ.replace(':%5B%22movie%22%5D,', ':%5B%22show%22%5D,'), genero, 'amazon', 'show'));
    }

    if (urlFilmin !== undefined) {
      const promisesFilmin = [];
      promisesFilmin.push(getPlatformUrls(urlFilmin, genero, 'filmin', 'movie'));
      promisesFilmin.push(getPlatformUrls(urlFilmin.replace(':%5B%22movie%22%5D,', ':%5B%22show%22%5D,'), genero, 'filmin', 'show'));
    }
  });
