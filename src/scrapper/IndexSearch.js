const axios = require('axios');
const cheerio = require('cheerio');
const MongoClient = require('mongodb').MongoClient,
  assert = require('assert');
const { index,algolia } = require('./firebase.js');
let bs = require('nodestalker'),
  client = bs.Client('127.0.0.1:11300');
const {
  fireStoreCollection, platforms, mongoCollection, mongoUrl,
} = require('../constants.js');


const indexSearch = (films) => {
  index
    .saveObjects(films)
    .then(() => {
      console.log('Contacts imported into Algolia');
    })
    .catch((error) => {
      console.error('Error when importing contact into Algolia', error);
      process.exit(1);
    });
};
const getPlatforms = (film) => {
  const selectedPlatforms = [];
  platforms.map((p) => {
    if (film[`platform_${p}`] !== undefined && film[`platform_${p}`] !== '') {
      selectedPlatforms.push(p);
    }
  });
  return selectedPlatforms;
};

algolia.deleteIndex('films', function(err, content) {
  MongoClient.connect(mongoUrl, (e, db) => {
    const collection = db.collection(mongoCollection);
    collection.find().toArray((errr, filmsList) => {
      const films = [];
      filmsList.map((f) => {
        const film = f;
        film.objectID = film.id;
        film.platformsSelected = getPlatforms(film);
        film.episodes=undefined;
        films.push(film);
      });
      indexSearch(films);
      db.close();
    });
  });
});
