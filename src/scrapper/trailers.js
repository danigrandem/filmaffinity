const axios = require('axios');
const cheerio = require('cheerio');
const MongoClient = require('mongodb').MongoClient,
  assert = require('assert');
let bs = require('nodestalker'),
  client = bs.Client('127.0.0.1:11300');
const {
  fireStoreCollection, generosValues, platformsValues, platforms, generosArray, mongoCollection, mongoUrl,
} = require('../constants.js');


const importTrailer = (film) => {
  const query = `${film.name} ${film.year} trailer`;
  const url = `https://www.googleapis.com/youtube/v3/search?part=id&q=${query.replace(/ /g, '+')}&type=video&key=AIzaSyAxTZ7EXtkk-NRcGw_hvyVsi3KLPEHQ-64`;
  console.log('url', url);
  return axios.get(url)
    .then((result) => {
      const trailer = result.data.items[0].id.videoId;
      console.log('trailer', trailer);

      client.use(mongoCollection).onSuccess((data) => {
        console.log(data);
        film.trailer = trailer;
        client.put(JSON.stringify(film)).onSuccess((data) => {
      	console.log(data);
      	// client.disconnect();
        });
      });
    });
};


MongoClient.connect(mongoUrl, (e, db) => {
  const collection = db.collection(mongoCollection);
  collection.find({ trailer: { $exists: false } }).toArray((errr, films) => {
    films.map((film) => {
      importTrailer(film);
    });
    db.close();
  });
});
