const axios = require('axios');
const cheerio = require('cheerio');
const request = require('request');
const slugify = require('slugify');
const { db } = require('./firebase.js');
const {
  baseFilmaffinityUrl, baseYomviUrl, baseNetflixUrl, baseHBOUrl, baseFilminUrl, baseAMZUrl, fireStoreCollection,
} = require('../constants.js');
const { Proxy } = require('./Proxy.js');


let url = '';
let urlNetflix = '';
let urlHBO = '';
let urlAMZ = '';
let urlFilmin = '';
let genero = '';
if (process.argv.length === 2) {
  console.log('Envia un genero: accion, animacion, aventura, scfi, independiente, infantil, drama, suspense, terror ...');
  process.exit();
} else {
  genero = process.argv[2];
  url = baseYomviUrl[genero].replace('/popular?','/new?');
  urlNetflix = baseNetflixUrl[genero].replace('/popular?','/new?');
  urlHBO = baseHBOUrl[genero].replace('/popular?','/new?');
  urlAMZ = baseAMZUrl[genero].replace('/popular?','/new?');
  urlFilmin = baseFilminUrl[genero].replace('/popular?','/new?');
}



const getPlatformUrls = (url, genero, type, showType) => axios.get(url).then((response) => {
  console.log(response.data.days[1].providers[0]);
  process.exit();
  response.data.items.map(f => axios.get(`https://apis.justwatch.com/content/titles/${showType}/${f.id}/locale/es_ES`).then((responsePlatforms) => {
  const platforms = {};
  responsePlatforms.data.offers.map((p) => {
    if (p.urls.standard_web !== undefined) {
      platforms[p.provider_id] = p.urls.standard_web;
    }
  });
  const director = getDirector(responsePlatforms.data.credits);
  const generoNombre = `genero_${genero}`;
  const fi = {
    id: `${f.id}`,
    name: f.title,
    description: f.short_description,
    year: f.original_release_year,
    image: `https://images.justwatch.com${f.poster.replace('{profile}', 's166')}`,
    [`platform_${type}`]: type,
    showType,
    director,
    platforms,
    [generoNombre]: genero,
  };
  films.push(f);
  getFilmAffinity(fi);
  getImdb(fi);
  //  console.log(films);
  return (films);
}))});


const batch = db.batch();
const generoQuery = `genero_${genero}`;
db.collection('films').where('is_active', '==', 1).where(generoQuery, '==', genero).get()
  .then((snapshot) => {
    console.log('EMPEZAMOS');
    /*  snapshot.forEach((doc) => {
      batch.set(doc._ref, { is_active: 0 }, { merge: true });
    });
    batch.commit(); */
    console.log('borrado finalizado');
    if (url !== undefined) {
      const promisesYomvi = [];
      promisesYomvi.push(getPlatformUrls(url, genero, 'yomvi', 'movie'));
      promisesYomvi.push(getPlatformUrls(url.replace(':%5B%22movie%22%5D,', ':%5B%22show%22%5D,'), genero, 'yomvi', 'show'));
    }
    if (urlNetflix !== undefined) {
      const promisesNetflix = [];
      promisesNetflix.push(getPlatformUrls(urlNetflix, genero, 'netflix', 'movie'));
      promisesNetflix.push(getPlatformUrls(urlNetflix.replace(':%5B%22movie%22%5D,', ':%5B%22show%22%5D,'), genero, 'netflix', 'show'));
    }

    if (urlHBO !== undefined) {
      const promisesHBO = [];
      promisesHBO.push(getPlatformUrls(urlHBO, genero, 'hbo', 'movie'));
      promisesHBO.push(getPlatformUrls(urlHBO.replace(':%5B%22movie%22%5D,', ':%5B%22show%22%5D,'), genero, 'hbo', 'show'));
    }

    if (urlAMZ !== undefined) {
      const promisesAMZ = [];
      promisesAMZ.push(getPlatformUrls(urlAMZ, genero, 'amazon', 'movie'));
      promisesAMZ.push(getPlatformUrls(urlAMZ.replace(':%5B%22movie%22%5D,', ':%5B%22show%22%5D,'), genero, 'amazon', 'show'));
    }

    if (urlFilmin !== undefined) {
      const promisesFilmin = [];
      promisesFilmin.push(getPlatformUrls(urlFilmin, genero, 'filmin', 'movie'));
      promisesFilmin.push(getPlatformUrls(urlFilmin.replace(':%5B%22movie%22%5D,', ':%5B%22show%22%5D,'), genero, 'filmin', 'show'));
    }
  });
