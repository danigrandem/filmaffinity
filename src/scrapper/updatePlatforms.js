const axios = require("axios");
const cheerio = require("cheerio");
const MongoClient = require("mongodb").MongoClient,
  assert = require("assert");
let bs = require("nodestalker");

const {
  getPlatformId,
  fireStoreCollection,
  generosValues,
  platformsValues,
  platforms,
  generosArray,
  mongoCollection,
  mongoUrl
} = require("../constants.js");

const getPlatformName = platform => {
  switch (platform) {
    case "8":
      return "netflix";
    case "118":
      return "hbo";
    case "149":
      return "yomvi";
    case "119":
      return "amazon";
    case "63":
      return "filmin";
    case "64":
      return "filmin";
    case "35":
      return "rakuten";
    default:
      return undefined;
  }
};
const platformsFormat = {
  platform_netflix: "",
  platform_hbo: "",
  platform_yomvi: "",
  platform_amazon: "",
  platform_filmin: ""
};
const updatePlatform = film =>
  axios
    .get(
      `https://apis.justwatch.com/content/titles/${film.showType}/${
        film.id_justwatch
      }/locale/es_ES`
    )
    .then(async result => {
      const initialPlatforms = Object.keys(film.platforms).reduce(
        (acc, curr) => {
          acc[curr] = curr === "149" ? film.platforms[curr] : "";
          return acc;
        },
        {}
      );
      const platformsAux =
        result.data.offers &&
        result.data.offers.reduce(
          (acc, curr, key) => {
            const platformName = getPlatformName(curr.provider_id.toString());
            //  console.log("platformName",platformName);
            if (platformName) {
              acc.platforms[curr.provider_id] = curr.urls.standard_web;
              acc[`platform_${platformName}`] = platformName;
            } else {
              acc[`platform_${platformName}`] = "";
            }
            return acc;
          },

          {
            ...platformsFormat,
            platforms: initialPlatforms,
            platform_yomvi: film.platform_yomvi ? film.platform_yomvi : ""
          }
        );
      if (platformsAux["platform_yomvi"] !== "") {
        let yomviReturn = null;
        try {
          yomviReturn = await axios({
            method: "head",
            url: `${platformsAux.platforms[149]}`
          });
        } catch (e) {}
        if (!yomviReturn || yomviReturn.status !== 200) {
          console.log("head 400", `${platformsAux.platforms[149]}`);
          platformsAux["platform_yomvi"] = "";
          delete platformsAux.platforms[149];
        }
      }
      const platforms = platformsAux || { ...platformsFormat };
      console.log("platforms", platforms);
      const client = bs.Client("127.0.0.1:11300");
      //  console.log('url################', `https://apis.justwatch.com/content/titles/${film.showType}/${film.id_justwatch}/locale/es_ES`);
      client.use(mongoCollection).onSuccess(data => {
        const newFilm = { ...film, ...platforms };
        client.put(JSON.stringify(newFilm)).onSuccess(data => {
          client.disconnect();
        });
      });
    })
    .catch(e => {
      const client = bs.Client("127.0.0.1:11300");
      client.use(mongoCollection).onSuccess(data => {
        console.log("se borra ", film.name);

        const newFilm = { ...film, platforms: {}, ...platformsFormat };
        client.put(JSON.stringify(newFilm)).onSuccess(data => {
          client.disconnect();
        });
      });
    });
MongoClient.connect(mongoUrl, (e, db) => {
  const collection = db.collection(mongoCollection);
    collection.find({ name: { $exists: true } }).toArray((errr, films) => {
  //collection.find({ id: "movie_30505" }).toArray((errr, films) => {
    films.map(film => {
      updatePlatform(film);
    });
    db.close();
  });
});
