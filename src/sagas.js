import { call, put, takeLatest } from 'redux-saga/effects';
import Api from './Api';


function* setChangeFilter(action) {
  yield put({
    type: 'SET_CHANGE_FILTER', changeFilter: action.changeFilter, platform: action.platform, note: action.note, genero: action.genero, year: action.year, showType: action.showType, order: action.order,
  });
}
function* setFilterModal(action) {
  yield put({ type: 'SET_FILTERMODAL', showFilterModal: action.showFilterModal });
}
// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* fetchFilms(action) {
  if (action.changeFilter !== undefined) {
    yield call(setChangeFilter, action);
  }
  const films = yield call(Api.fetchFilms, action.platform, action.note, action.genero, action.year, action.showType, action.limit, action.order);
  yield put({
    type: 'SET_FILMS', films, platform: action.platform, note: action.note, genero: action.genero, year: action.year, showType: action.showType, order: action.order,
  });
}
function* fetchFilm(action) {
  const film = yield call(Api.fetchFilm, action.idFicha);
  yield put({
    type: 'SET_ITEM_VIEW_SEARCH', film,
  });
}
function* setItemSearch(action) {
  const films = yield call(Api.search, action.value);
  yield put({
    type: 'SET_ITEM_SEARCH', films, value: action.value,
  });
}
function* setFilters(action) {
  yield put({
    type: 'SET_FILTERS', platform: action.platform, note: action.note, genero: action.genero, year: action.year, showType: action.showType, order: action.order,
  });
}
function* setInProgress(action) {
  yield put({ type: 'SET_IN_PROGRESS', inProgress: action.inProgress });
}
function* setShowList(action) {
  yield put({ type: 'SHOW_LIST', showList: action.showList });
}
function* setIsSsr(action) {
  yield put({ type: 'SET_IS_SSR', isSsr: action.isSsr });
}
function* setIdFilm(action) {
  yield put({ type: 'SET_ID_FILM', idFilm: action.idFilm });
}
function* setItemView(action) {
  yield put({ type: 'SET_ITEM_VIEW', idFilm: action.idFilm });
}
function* setItemViewSearch(action) {
  yield put({ type: 'SET_ITEM_VIEW_SEARCH', film: action.film });
}
function* setIdFilmMore(action) {
  yield put({ type: 'SET_ID_FILM_MORE', idFilmMore: action.idFilmMore });
}
function* setUserAction(action) {
  const url = action.actionType === 'add' ? Api.addToList : Api.removeFromList;
  const films = yield call(url, action.email, 'tosee', action.idFilm);
  yield put({
    type: 'SET_USER_ACTION', films, actionType: "tosee", email: action.email,
  });
}
/*
  Starts fetchUser on each dispatched `USER_FETCH_REQUESTED` action.
  Allows concurrent fetches of user.
*/
function* mySaga() {
  yield takeLatest('USER_FETCH_REQUESTED', fetchFilms);
  yield takeLatest('FILM_FETCH_REQUEST', fetchFilm);
  yield takeLatest('SHOW_FILTER_MODAL', setFilterModal);
  yield takeLatest('SET_SHOW_LIST', setShowList);
  yield takeLatest('SET_FILTERS_FETCH', setFilters);
  yield takeLatest('USER_PAGINATE_PROGRESS', setInProgress);
  yield takeLatest('HIDE_LOADING', setIsSsr);
  yield takeLatest('PLAY_OPTIONS', setIdFilm);
  yield takeLatest('ITEM_VIEW', setItemView);
  yield takeLatest('ITEM_SEARCH', setItemSearch);
  yield takeLatest('ITEM_VIEW_SEARCH', setItemViewSearch);
  yield takeLatest('SHOW_MORE_INFO', setIdFilmMore);
  yield takeLatest('FILTER_IN_PROGRESS', setChangeFilter);
  yield takeLatest('ON_USER_ACTION', setUserAction);
}

export default mySaga;
