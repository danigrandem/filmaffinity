import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import App from './containers/App';
import Notfound from './components/Notfound';


export const routes = (
  <Switch>
    <Route exact path="/" component={App} />
    <Route exact path="/ficha/:idFicha" component={App} />
    <Route exact path="/tipo/:show/plataforma/:plataforma" component={App} />
    <Route exact path="/plataforma/:plataforma" component={App} />
    <Route exact path="/tipo/:show" component={App} />
    <Route exact path="/year/:year" component={App} />
    <Route exact path="/tipo/:show/year/:year" component={App} />
    <Route exact path="/plataforma/:plataforma/nota/:nota" component={App} />
    <Route exact path="/tipo/:show/plataforma/:plataforma/nota/:nota" component={App} />
    <Route exact path="/plataforma/:plataforma/nota/:nota/year/:year" component={App} />
    <Route exact path="/tipo/:show/plataforma/:plataforma/nota/:nota/year/:year" component={App} />
    <Route exact path="/plataforma/:plataforma/genero/:genero" component={App} />
    <Route exact path="/tipo/:show/plataforma/:plataforma/genero/:genero" component={App} />
    <Route exact path="/plataforma/:plataforma/genero/:genero/year/:year" component={App} />
    <Route exact path="/tipo/:show/plataforma/:plataforma/genero/:genero/year/:year" component={App} />
    <Route exact path="/plataforma/:plataforma/year/:year" component={App} />
    <Route exact path="/tipo/:show/plataforma/:plataforma/year/:year" component={App} />
    <Route exact path="/plataforma/:plataforma/nota/:nota/genero/:genero" component={App} />
    <Route exact path="/tipo/:show/plataforma/:plataforma/nota/:nota/genero/:genero" component={App} />
    <Route exact path="/plataforma/:plataforma/nota/:nota/genero/:genero/year/:year" component={App} />
    <Route exact path="/tipo/:show/plataforma/:plataforma/nota/:nota/genero/:genero/year/:year" component={App} />
    <Route exact path="/nota/:nota" component={App} />
    <Route exact path="/tipo/:show/nota/:nota" component={App} />
    <Route exact path="/nota/:nota/genero/:genero" component={App} />
    <Route exact path="/tipo/:show/nota/:nota/genero/:genero" component={App} />
    <Route exact path="/nota/:nota/year/:year" component={App} />
    <Route exact path="/tipo/:show/nota/:nota/year/:year" component={App} />
    <Route exact path="/nota/:nota/genero/:genero/year/:year" component={App} />
    <Route exact path="/tipo/:show/nota/:nota/genero/:genero/year/:year" component={App} />
    <Route exact path="/genero/:genero" component={App} />
    <Route exact path="/tipo/:show/genero/:genero" component={App} />
    <Route exact path="/genero/:genero/year/:year" component={App} />
    <Route exact path="/tipo/:show/genero/:genero/year/:year" component={App} />
    <Route component={Notfound} />
  </Switch>
);

export const Status = ({ code, children }) => (
  <Route render={({ staticContext }) => {
    if (staticContext) {
      staticContext.status = code;
    }
    return children;
  }}
  />
);

Status.propTypes = {
  code: PropTypes.number.isRequired,
  children: PropTypes.any.isRequired,
};
