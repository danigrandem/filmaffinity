import auth0 from 'auth0-js';

export default class Auth {
  auth0 = new auth0.WebAuth({
    domain: 'filmchart.eu.auth0.com',
    clientID: 'hY8wi1qQIma9TTn2VjSqSJx8SiwPHFnv',
    redirectUri: 'http://localhost:3000/callback',
    audience: 'https://filmchart.eu.auth0.com/userinfo',
    responseType: 'token id_token',
    scope: 'openid',
  });

  login() {
    this.auth0.authorize();
  }
}
