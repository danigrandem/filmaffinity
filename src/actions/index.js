export const setFilms = (films, platform = 'all', note = 'all', genero = 'all', year = 'all', showType = 'all') => (
  {
    type: 'SET_FILMS',
    films,
    platform,
    note,
    genero,
    year,
    showType,
  }
);
export const setFilterModal = showFilterModal => (
  {
    type: 'SET_FILTERMODAL',
    showFilterModal,
  }
);

export const setUserAction = (films, actionType, email) => (
  {
    type: 'SET_USER_ACTION',
    films,
    actionType,
    email,
  }
);
export const setChangeFilter = (changeFilter, platform, note, genero, year, showType, order = 'note_filmaffinity') => (
  {
    type: 'SET_CHANGE_FILTER',
    changeFilter,
    platform,
    note,
    genero,
    year,
    showType,
    order,
  }
);
export const setIsssr = isSsr => (
  {
    type: 'SET_IS_SSR',
    isSsr,
  }
);
export const showList = showList => (
  {
    type: 'SHOW_LIST',
    showList,
  }
);
export const setOrder = order => (
  {
    type: 'SET_ORDER',
    order,
  }
);
export const setIdFilm = idFilm => (
  {
    type: 'SET_ID_FILM',
    idFilm,
  }
);
export const setItemView = idFilm => (
  {
    type: 'SET_ITEM_VIEW',
    idFilm,
  }
);
export const setItemSearch = (films, value) => (
  {
    type: 'SET_ITEM_SEARCH',
    films,
    value,

  }
);
export const setItemViewSearch = film => (
  {
    type: 'SET_ITEM_VIEW_SEARCH',
    film,
  }
);
export const setIdFilmMore = idFilmMore => (
  {
    type: 'SET_ID_FILM_MORE',
    idFilmMore,
  }
);
export const setFilters = (platform, note, genero, year, showType, order = 'note_filmaffinity') => (
  {
    type: 'SET_FILTERS',
    platform,
    note,
    genero,
    year,
    showType,
    order,
  }
);
export const filmFetch = () => (
  {
    type: 'COMPANY_FETCH_REQUESTED',
  }
);
