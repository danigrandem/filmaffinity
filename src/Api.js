import axios from "axios";

import { perPage, platforms, getPlatformId } from "./constants";

class Api {
  static getPlatforms(film) {
    const selectedPlatforms = [];
    platforms.map(p => {
      const platformId =
        p === "filmin"
          ? film.platforms[getPlatformId(p)] || film.platforms[64]
          : film.platforms[getPlatformId(p)];
      if (platformId) {
        selectedPlatforms.push(p);
      }
    });
    return selectedPlatforms;
  }
  static fetchFilms(
    platform = "all",
    note = "7",
    genero = "all",
    year = "all",
    showType = "all",
    limit = perPage,
    order = "new_releases"
  ) {
    return axios
      .get(
        `https://filmchart.es/api/get-list/?platform=${platform}&note=${note}&genero=${genero}&limit=${limit}&year=${year}&showType=${showType}&order=${order}`
      )
      .then(result => {
        const films = [];
        result.data.map(f => {
          const filmaffinity = [];
          const imdb = [];
          if (f.filmaffinity !== undefined && f.filmaffinity.length > 0) {
            f.filmaffinity.map(fa => {
              filmaffinity.push({
                id: fa.id,
                img: fa.img,
                name: fa.name,
                note: fa.note,
                year: fa.year,
                url: fa.url
              });
            });
          }
          if (f.imdb !== undefined) {
            if (f.imdb.length > 0) {
              f.imdb.map(i => {
                imdb.push({
                  id: i.id,
                  img: i.img,
                  name: i.name,
                  note: i.note,
                  year: i.year,
                  url: i.url
                });
              });
            }
          }
          const elem = Object.assign(
            f,
            { platformsSelected: Api.getPlatforms(f) },
            { filmaffinity },
            { imdb }
          );
          films.push(elem);
        });
        return films;
      });
  }

  static search(value) {
    return axios
      .get(`https://filmchart.es/api/search?value=${value}`)
      .then(result => {
        const films = [];
        result.data.map(f => {
          const filmaffinity = [];
          const imdb = [];
          if (f.filmaffinity !== undefined && f.filmaffinity.length > 0) {
            f.filmaffinity.map(fa => {
              filmaffinity.push({
                id: fa.id,
                img: fa.img,
                name: fa.name,
                note: fa.note,
                year: fa.year,
                url: fa.url
              });
            });
          }
          if (f.imdb !== undefined) {
            if (f.imdb.length > 0) {
              f.imdb.map(i => {
                imdb.push({
                  id: i.id,
                  img: i.img,
                  name: i.name,
                  note: i.note,
                  year: i.year,
                  url: i.url
                });
              });
            }
          }
          const elem = Object.assign(
            f,
            { platformsSelected: Api.getPlatforms(f) },
            { filmaffinity },
            { imdb }
          );
          films.push(elem);
        });
        return films;
      });
  }
  static fetchFilm(idFicha) {
    return axios
      .get(`https://filmchart.es/api/get-ficha/?id=${idFicha}`)
      .then(result => {
        const films = [];
        result.data.map(f => {
          const filmaffinity = [];
          const imdb = [];
          if (f.filmaffinity !== undefined && f.filmaffinity.length > 0) {
            f.filmaffinity.map(fa => {
              filmaffinity.push({
                id: fa.id,
                img: fa.img,
                name: fa.name,
                note: fa.note,
                year: fa.year,
                url: fa.url
              });
            });
          }
          if (f.imdb !== undefined) {
            if (f.imdb.length > 0) {
              f.imdb.map(i => {
                imdb.push({
                  id: i.id,
                  img: i.img,
                  name: i.name,
                  note: i.note,
                  year: i.year,
                  url: i.url
                });
              });
            }
          }
          const elem = Object.assign(
            f,
            { platformsSelected: Api.getPlatforms(f) },
            { filmaffinity },
            { imdb }
          );
          films.push(elem);
        });
        return films.length > 0 ? films[0] : false;
      });
  }

  static addToList(email, actionType, idFilm) {
    return axios
      .get(
        `https://filmchart.es/api/add-to-list/?email=${email}&id=${idFilm}&&listName=${actionType}`
      )
      .then(result => {
        const films = [];
        result.data.map(f => {
          const filmaffinity = [];
          const imdb = [];
          if (f.filmaffinity !== undefined && f.filmaffinity.length > 0) {
            f.filmaffinity.map(fa => {
              filmaffinity.push({
                id: fa.id,
                img: fa.img,
                name: fa.name,
                note: fa.note,
                year: fa.year,
                url: fa.url
              });
            });
          }
          if (f.imdb !== undefined) {
            if (f.imdb.length > 0) {
              f.imdb.map(i => {
                imdb.push({
                  id: i.id,
                  img: i.img,
                  name: i.name,
                  note: i.note,
                  year: i.year,
                  url: i.url
                });
              });
            }
          }
          const elem = Object.assign(
            f,
            { platformsSelected: Api.getPlatforms(f) },
            { filmaffinity },
            { imdb }
          );
          films.push(elem);
        });
        return films;
      });
  }

  static removeFromList(email, actionType, idFilm) {
    return axios
      .get(
        `https://filmchart.es/api/remove-from-list/?email=${email}&id=${idFilm}&&listName=${actionType}`
      )
      .then(result => {
        const films = [];
        result.data.map(f => {
          const filmaffinity = [];
          const imdb = [];
          if (f.filmaffinity !== undefined && f.filmaffinity.length > 0) {
            f.filmaffinity.map(fa => {
              filmaffinity.push({
                id: fa.id,
                img: fa.img,
                name: fa.name,
                note: fa.note,
                year: fa.year,
                url: fa.url
              });
            });
          }
          if (f.imdb !== undefined) {
            if (f.imdb.length > 0) {
              f.imdb.map(i => {
                imdb.push({
                  id: i.id,
                  img: i.img,
                  name: i.name,
                  note: i.note,
                  year: i.year,
                  url: i.url
                });
              });
            }
          }
          const elem = Object.assign(
            f,
            { platformsSelected: Api.getPlatforms(f) },
            { filmaffinity },
            { imdb }
          );
          films.push(elem);
        });
        return films;
      });
  }

  static getUserLists(email) {
    return axios
      .get(`https://filmchart.es/api/get-user-list/?email=${email}`)
      .then(result => {
        const films = [];
        result.data.map(f => {
          const filmaffinity = [];
          const imdb = [];
          if (f.filmaffinity !== undefined && f.filmaffinity.length > 0) {
            f.filmaffinity.map(fa => {
              filmaffinity.push({
                id: fa.id,
                img: fa.img,
                name: fa.name,
                note: fa.note,
                year: fa.year,
                url: fa.url
              });
            });
          }
          if (f.imdb !== undefined) {
            if (f.imdb.length > 0) {
              f.imdb.map(i => {
                imdb.push({
                  id: i.id,
                  img: i.img,
                  name: i.name,
                  note: i.note,
                  year: i.year,
                  url: i.url
                });
              });
            }
          }
          const elem = Object.assign(
            f,
            { platformsSelected: Api.getPlatforms(f) },
            { filmaffinity },
            { imdb }
          );
          films.push(elem);
        });
        return films;
      })
      .catch(e => {
        console.log(e);
      });
  }
}

export default Api;
