import { fromJS } from "immutable";

const states = fromJS({
  films: {},
  idFilm: "0",
  idFilmMore: "0",
  filters: {
    order: "new_releases"
  },
  showFilterModal: false,
  inProgress: false,
  isSsr: false,
  search: {},
  isWebView: false,
  changeFilter: false,
  filmView: false,
  hasMore: {},
  showList: false,
  user: {
    email: "",
    avatar: "",
    name: "",
    seen: [],
    tosee: []
  }
});
const getFilm = (idFilm, state) => {
  console.log("entra",state.get("showList"));
  const filmList = state.get("showList")?state.getIn(["user","tosee"]):state
    .getIn([
      "films",
      state.getIn(["filters", "showType"]),
      state.getIn(["filters", "genero"]),
      state.getIn(["filters", "platform"]),
      state.getIn(["filters", "note"]),
      state.getIn(["filters", "year"]),
      state.getIn(["filters", "order"])
    ]);

  let film = false;
  filmList
    .map(f => {
      if (f.get("id") === idFilm) {
        film = f;
      }
    });
    console.log("sdsadsadsds",film);
  return film;
};
const reducers = (state = states, action) => {
  switch (action.type) {
    case "SET_FILMS":
      const total =
        state.getIn([
          "films",
          action.showType,
          action.genero,
          action.platform,
          action.note,
          action.year,
          action.order
        ]) !== undefined
          ? state.getIn([
              "films",
              action.showType,
              action.genero,
              action.platform,
              action.note,
              action.year,
              action.order
            ]).size
          : 0;
      return state
        .setIn(
          [
            "films",
            action.showType,
            action.genero,
            action.platform,
            action.note,
            action.year,
            action.order
          ],
          fromJS(action.films)
        )
        .setIn(
          [
            "hasMore",
            action.showType,
            action.genero,
            action.platform,
            action.note,
            action.year,
            action.order
          ],
          fromJS(total !== action.films.length)
        )
        .setIn(["filters", "genero"], fromJS(action.genero))
        .setIn(["filters", "platform"], fromJS(action.platform))
        .setIn(["filters", "note"], fromJS(action.note))
        .setIn(["filters", "year"], fromJS(action.year))
        .setIn(["filters", "showType"], fromJS(action.showType))
        .setIn(["filters", "order"], fromJS(action.order))
        .set("inProgress", fromJS(false))
        .set("changeFilter", fromJS(false))
        .set("filmView", fromJS(false))
        .set("showList", fromJS(false))
        .set("showFilterModal", fromJS(false));
    case "SET_FILTERMODAL":
      return state.set("showFilterModal", fromJS(action.showFilterModal));
      case "SHOW_LIST":
        return state.set("filmView", fromJS(false)).set("showList", fromJS(action.showList));
    case "SET_CHANGE_FILTER":
      return state
        .set("changeFilter", fromJS(action.changeFilter))
        .setIn(["filters", "platform"], fromJS(action.platform))
        .setIn(["filters", "note"], fromJS(action.note))
        .setIn(["filters", "year"], fromJS(action.year))
        .setIn(["filters", "genero"], fromJS(action.genero))
        .setIn(["filters", "showType"], fromJS(action.showType))
        .setIn(["filters", "order"], fromJS(action.order))
        .set("showFilterModal", fromJS(false))
        .set("showList", fromJS(false));
    case "SET_IS_SSR":
      return state.set("isSsr", fromJS(action.isSsr));
    case "SET_ID_FILM":
      return state.set("idFilm", fromJS(action.idFilm)).set("showList", fromJS(false));
    case "SET_ITEM_VIEW":
      return state.set("filmView", fromJS(getFilm(action.idFilm, state)));
    case "SET_ITEM_VIEW_SEARCH":
      return state.set("showList", fromJS(false)).set("filmView", fromJS(action.film));
    case "SET_ITEM_SEARCH":
      return state.set("showList", fromJS(false)).setIn(["search", action.value], fromJS(action.films));
    case "SET_ID_FILM_MORE":
      return state.set("showList", fromJS(false)).set("idFilmMore", fromJS(action.idFilmMore));
    case "SET_IN_PROGRESS":
      return state.set("showList", fromJS(false)).set("inProgress", fromJS(action.inProgress));
    case "SET_USER_ACTION":
      return state.set("showList", fromJS(false)).setIn(["user", action.actionType], fromJS(action.films));
    case "SET_FILTERS":
      return state
        .setIn(["filters", "platform"], fromJS(action.platform))
        .setIn(["filters", "note"], fromJS(action.note))
        .setIn(["filters", "year"], fromJS(action.year))
        .setIn(["filters", "genero"], fromJS(action.genero))
        .setIn(["filters", "showType"], fromJS(action.showType))
        .setIn(["filters", "order"], fromJS(action.order))
        .set("filmView", fromJS(false))
        .set("showList", fromJS(false))
        .set("showFilterModal", fromJS(false));
    default:
      return state;
  }
};

export default reducers;
