let bs = require("nodestalker"),
  client = bs.Client("127.0.0.1:11300");

const MongoClient = require("mongodb").MongoClient,
  assert = require("assert");

const {
  baseFilmaffinityUrl,
  baseYomviUrl,
  baseNetflixUrl,
  baseHBOUrl,
  baseFilminUrl,
  baseAMZUrl,
  mongoUrl,
  mongoCollection
} = require("./src/constants.js");

client.watch(mongoCollection).onSuccess(data => {
  function resJob() {
    client.reserve().onSuccess(job => {
      // console.log('reserved', JSON.parse(job.data));
      const elem = JSON.parse(job.data);
      MongoClient.connect(mongoUrl, (e, db) => {
        const collection = db.collection(mongoCollection);
        //  console.log("elem",elem);
        const elemAux = Object.assign({}, elem);
        delete elem._id;

        const platformsFormatted = elem.platforms
          ? Object.keys(elem.platforms)
              .filter(key => ["8", "118", "149", "119", "63", "61","64", "35"].includes(key))
              .reduce((acc, curr, key) => {
                acc[`platforms.${curr}`] = elem.platforms[curr];
                return acc;
              }, {})
          : {};
        collection.update(
          { _id: elemAux._id },
          {
            $set: { ...platformsFormatted }
          },
          { upsert: true },
          (error, task) => {
            const isModified = !!task.result.upserted;
            console.log(isModified);
            if (error) {
              console.log("error");
            } else {
              console.log("isModified", task.result.nModified);
              console.log("upserted", task.result.upserted);
              const { platforms, ...newElem } = elem;
              collection.update(
                { _id: elemAux.id },
                {
                  $set: {
                    ...newElem,
                    ...( isModified
                      ? { inserted_at: new Date().getTime() }
                      : {})
                  }
                },
                { upsert: true },
                (error2, task2) => {
                  if (error) {
                    console.log(error);
                  }
                  if (isModified)
                    console.log("peli nuevaaaaaaaaaaaaaaaaa", elem.id);
                }
              );
            }

            console.log("insertado", elem.id);
          }
        );
      });

      client.deleteJob(job.id).onSuccess(del_msg => {
        //  console.log('deleted', job);
        //  console.log('message', del_msg);
        resJob();
      });
    });
  }

  resJob();
});
