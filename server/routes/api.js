import express from "express";

const MongoClient = require("mongodb").MongoClient,
  assert = require("assert");

const router = express.Router();
const {
  fireStoreCollection,
  mongoUrl,
  mongoCollection,
  getPlatformId
} = require("../../src/constants.js");

router.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
const formatFilters = req => {
  const { platform, note, genero, limit, year, showType, order } = req.query;
  const filters = [{ is_active: 1 }];
  if (showType !== "all") {
    let showTypeFinal = {};
    const showTypeArr = showType.split(";");
    if (showTypeArr.length > 1) {
      const showTypeArray = [];
      showTypeArr.map(s => {
        showTypeArray.push({
          showType: s
        });
      });
      showTypeFinal = {
        $or: showTypeArray
      };
      filters.push(showTypeFinal);
    } else {
      filters.push({ showType });
    }
  }
  if (platform !== "all") {
    const platformArr = platform.split(";");
    let platformFinal = {};
    if (platformArr.length > 1) {
      const platformsArray = [];
      platformArr.map(p => {
        const platformVar = `platforms.${getPlatformId(p)}`;
        const objectAux = {};
        objectAux[platformVar] = { $exists: true, $ne: "" };

        platformsArray.push(objectAux);
        if (p === "filmin") {
          platformsArray.push({ "platforms.64": { $exists: true, $ne: "" } });
        }
      });
      platformFinal = {
        $or: platformsArray
      };
    } else {
      const platformVar = `platforms.${getPlatformId(platform)}`;

      if (platform === "filmin") {
        const platformsArray = [];
        platformsArray.push({ "platforms.63": { $exists: true, $ne: "" } });
        platformsArray.push({ "platforms.64": { $exists: true, $ne: "" } });

        platformFinal = {
          $or: platformsArray
        };
      } else {
        platformFinal[platformVar] = { $exists: true, $ne: "" };
      }
    }
    filters.push(platformFinal);
  }
  if (genero !== "all") {
    const generoArr = genero.split(";");
    let generoFinal = {};
    if (generoArr.length > 1) {
      const generosArray = [];
      generoArr.map(g => {
        const generoVar = `genero_${g}`;
        const objectAux = {};
        objectAux[generoVar] = g;
        generosArray.push(objectAux);
      });
      generoFinal = {
        $or: generosArray
      };
    } else {
      const generoVar = `genero_${genero}`;
      generoFinal[generoVar] = genero;
    }
    filters.push(generoFinal);
  }
  if (year !== "all") {
    const yearArr = year.split(";");
    let yearFinal = {};
    if (yearArr.length > 1) {
      const yearsArray = [];
      yearArr.map(y => {
        yearsArray.push({
          year: parseInt(y)
        });
      });
      yearFinal = {
        $or: yearsArray
      };
    } else {
      yearFinal = {
        year: parseInt(year)
      };
    }

    filters.push(yearFinal);
  }
  if (note !== "all") {
    const noteArr = note.split(";");
    let noteFinal = {};

    if (noteArr.length > 1) {
      const noteArray = [];
      noteArr.map(n => {
        const objectFinal = {};
        const minVal = parseInt(n) - 0.1;
        const maxVal = parseInt(n) + 1;
        objectFinal[order === "new_releases" ? "note_filmaffinity" : order] = {
          $gt: minVal,
          $lt: maxVal
        };
        noteArray.push(objectFinal);
      });
      noteFinal = {
        $or: noteArray
      };
    } else {
      const minVal = parseInt(note) - 0.1;
      const maxVal = parseInt(note) + 1;
      noteFinal[order === "new_releases" ? "note_filmaffinity" : order] = {
        $gt: minVal,
        $lt: maxVal
      };
    }
    filters.push(noteFinal);
  }
  filters.push({ platforms: { $gt: {} } });
  return filters;
};
router.get("/get-list/", (req, res) => {
  const { platform, note, genero, limit, year, showType, order } = req.query;

  const sortField = {};

  sortField[order === "new_releases" ? "inserted_at" : order] = -1;
  MongoClient.connect(mongoUrl, (e, db) => {
    const filters = formatFilters(req);
    const collection = db.collection(mongoCollection);
    collection
      .find({
        $and: filters
      })
      .sort(sortField)
      .limit(parseInt(limit))
      .toArray((errr, films) => {
        res.json(films);
        db.close();
      });
  });
});

router.get("/get-ficha/", (req, res) => {
  const { id } = req.query;

  MongoClient.connect(mongoUrl, (e, db) => {
    const collection = db.collection(mongoCollection);
    collection
      .find({
        id
      })
      .limit(parseInt(1))
      .toArray((errr, item) => {
        res.json(item);
        db.close();
      });
  });
});

router.get("/search/", (req, res) => {
  const { value } = req.query;

  MongoClient.connect(mongoUrl, (e, db) => {
    const collection = db.collection(mongoCollection);
    collection
      .find(
        { $text: { $search: `\"${value}\"` } },
        { score: { $meta: "textScore" } }
      )
      .sort({ score: { $meta: "textScore" } })
      .limit(parseInt(10))
      .toArray((errr, item) => {
        res.json(item);
        db.close();
      });
  });
});

router.get("/get-user-list/", (req, res) => {
  const { email } = req.query;

  MongoClient.connect(mongoUrl, (e, db) => {
    const collection = db.collection(mongoCollection);
    collection.find({ list: `${email}` }).toArray((errr, item) => {
      if (item.length === 0 || !item[0].tosee) {
        db.close();
        return res.json([]);
      }
      collection.find({ id: { $in: item[0].tosee } }).toArray((errr, items) => {
        res.json(items);
        db.close();
      });
    });
  });
});

router.get("/add-to-list/", (req, res) => {
  const { listName, id, email } = req.query;

  MongoClient.connect(mongoUrl, (e, db) => {
    const collection = db.collection(mongoCollection);
    collection.update(
      { list: `${email}` },
      { $addToSet: { [listName]: id } },
      { upsert: true },
      (err, task) => {
        collection.find({ list: `${email}` }).toArray((errr, item) => {
          if (item.length === 0 || !item[0].tosee) return res.json([]);
          collection
            .find({ id: { $in: item[0].tosee } })
            .toArray((errr, items) => {
              res.json(items);
              db.close();
            });
        });
      }
    );
  });
});

router.get("/remove-from-list/", (req, res) => {
  const { listName, id, email } = req.query;

  MongoClient.connect(mongoUrl, (e, db) => {
    const collection = db.collection(mongoCollection);
    collection.update(
      { list: `${email}` },
      { $pull: { [listName]: id } },
      (err, task) => {
        collection.find({ list: `${email}` }).toArray((errr, item) => {
          if (item.length === 0 || !item[0].tosee) return res.json([]);
          collection
            .find({ id: { $in: item[0].tosee } })
            .toArray((errr, items) => {
              res.json(items);
              db.close();
            });
        });
      }
    );
  });
});

export default router;
