import path from 'path';
import fs from 'fs';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { ServerStyleSheet } from 'styled-components';
import Helmet from 'react-helmet';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { routes } from '../src/routes';
import reducers from '../src/reducers/index';
import Api from '../src/Api';
import mySaga from '../src/sagas';


const { fromJS } = require('immutable');


const sagaMiddleware = createSagaMiddleware();

// A simple helper function to prepare the HTML markup
const prepHTML = (data, {
  html, head, body, preloadedState,
}) => {
  data = data.replace('<html lang="en">', `<html ${html}`);
  data = data.replace('</head>', `${head}</head>`);
  data = data.replace('<div id="root"></div>', `<div id="root">${body}<script>window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}</script></div>`);

  return data;
};
const getInitialState = async (data, params, req, isFicha) => {
  const finalData = {};
  if (isFicha === false) {
    finalData[params.showType] = {};
    finalData[params.showType][params.genero] = {};
    finalData[params.showType][params.genero][params.platform] = {};
    finalData[params.showType][params.genero][params.platform][params.note] = {};
    finalData[params.showType][params.genero][params.platform][params.note][params.year] = {};
    finalData[params.showType][params.genero][params.platform][params.note][params.year].new_releases = data;
  }
  const hasMore = {};
  if (isFicha === false) {
    hasMore[params.showType] = {};
    hasMore[params.showType][params.genero] = {};
    hasMore[params.showType][params.genero][params.platform] = {};
    hasMore[params.showType][params.genero][params.platform][params.note] = {};
    hasMore[params.showType][params.genero][params.platform][params.note][params.year] = {};
    hasMore[params.showType][params.genero][params.platform][params.note][params.year].new_releases = true;
  }
  const isWebView = (req.get('User-Agent') !== undefined) ? req.get('User-Agent').includes('webviewAndroid') : false;
  const tosee = req.headers.email ? await Api.getUserLists(req.headers.email) : [];
  const states = fromJS({
    films: finalData,
    idFilm: '0',
    idFilmMore: '0',
    filters: params,
    showFilterModal: false,
    inProgress: false,
    isSsr: true,
    isWebView,
    hasMore,
    search: {},
    filmView: (isFicha) ? data : false,
    changeFilter: false,
    showList:false,
    user: {
      email: req.headers.email || '',
      avatar: req.headers.profileimg || '',
      name: req.headers.fullname || '',
      seen: [],
      tosee,
    },
  });

  return states;
};
const getParams = (req) => {
  const params = req.url.split('/');
  const paramReturn = {};
  if (params[1] === 'ficha') {
    paramReturn.platform = 'all';
    paramReturn.genero = 'all';
    paramReturn.note = 'all';
    paramReturn.year = 'all';
    paramReturn.showType = 'all';
    paramReturn.order = 'new_releases';
    return paramReturn;
  }
  switch (params.length) {
    case 3:
      paramReturn[params[1]] = params[2];
      break;
    case 5:
      paramReturn[params[1]] = params[2];
      paramReturn[params[3]] = params[4];

      break;
    case 7:
      paramReturn[params[1]] = params[2];
      paramReturn[params[3]] = params[4];
      paramReturn[params[5]] = params[6];

      break;
    case 9:
      paramReturn[params[1]] = params[2];
      paramReturn[params[3]] = params[4];
      paramReturn[params[5]] = params[6];
      paramReturn[params[7]] = params[8];

      break;
    case 11:
      paramReturn[params[1]] = params[2];
      paramReturn[params[3]] = params[4];
      paramReturn[params[5]] = params[6];
      paramReturn[params[7]] = params[8];
      paramReturn[params[9]] = params[10];

      break;
    default:
      paramReturn.platform = 'all';
      paramReturn.genero = 'all';
      paramReturn.note = 'all';
      paramReturn.year = 'all';
      paramReturn.showType = 'all';
  }
  const platform = (paramReturn.plataforma !== undefined) ? paramReturn.plataforma : 'all';
  const genero = (paramReturn.genero !== undefined) ? paramReturn.genero : 'all';
  const note = (paramReturn.nota !== undefined) ? paramReturn.nota : 'all';
  const year = (paramReturn.year !== undefined) ? paramReturn.year : 'all';
  const showType = (paramReturn.tipo !== undefined) ? paramReturn.tipo : 'all';
  return {
    platform,
    genero,
    note,
    year,
    order: 'new_releases',
    showType,
  };
};
const universalLoader = (req, res) => {
  // Load in our HTML file from our build
  // console.log("req",req.headers);
  const filePath = path.resolve(__dirname, '../build/index.html');
  fs.readFile(filePath, 'utf8', (err, htmlData) => {
    const params = getParams(req);
    const isFicha = req.url.split('/')[1] === 'ficha';
    const idFicha = (isFicha) ? req.url.split('/')[2] : false;
    const promises = (isFicha) ? Api.fetchFilm(idFicha)
      : Api.fetchFilms(params.platform, params.note, params.genero, params.year, params.showType);
    Promise.all([promises]).then(async (data) => {
      const states = await getInitialState(data[0], params, req, isFicha);
      const store = createStore(reducers, states, applyMiddleware(sagaMiddleware));
      sagaMiddleware.run(mySaga);
      const context = {};
      const sheet = new ServerStyleSheet();
      const routeMarkup = renderToString(<Provider store={store}>
        <StaticRouter location={req.url} context={context}>
          {routes}
        </StaticRouter>
      </Provider>);

      const status = (context.status !== undefined) ? context.status : 200;
      const helmet = Helmet.renderStatic();
      const styles = sheet.getStyleTags();
      const html = prepHTML(htmlData, {
        html: helmet.htmlAttributes.toString(),
        head:
          helmet.title.toString() +
          helmet.meta.toString() +
          helmet.link.toString() +
          styles,
        body: routeMarkup,
        preloadedState: store.getState(),
      });
      res.status(status).send(html);
    });
  });
};

export default universalLoader;
