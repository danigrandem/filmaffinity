import bodyParser from 'body-parser';
import compression from 'compression';
import express from 'express';
import path from 'path';
import index from './routes/index';
import api from './routes/api';
import universalLoader from './universal';

const app = express();
const PORT = process.env.PORT || 3000;

app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', index);
app.use('/api', api);
app.use(express.static(path.resolve(__dirname, '../build')));
app.use(express.static(path.resolve(__dirname, '../media')));
app.use('/', universalLoader);

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}!`);
});
